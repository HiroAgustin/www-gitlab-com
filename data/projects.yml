gitlab-ce:
  name: GitLab Community Edition (CE)
  path: gitlab-org/gitlab-ce
  link: https://gitlab.com/gitlab-org/gitlab-ce
  mirrors:
    - https://github.com/gitlabhq/gitlabhq
    - https://dev.gitlab.org/gitlab/gitlabhq
  description: |
    This is the community edition of GitLab.

    Most of the development happens here, then gets merged into GitLab EE
    periodically. Unless you're making a change specific to GitLab EE, add it to CE.

gitlab-ee:
  name: GitLab Enterprise Edition (EE)
  path: gitlab-org/gitlab-ee
  link: https://gitlab.com/gitlab-org/gitlab-ee
  description: |
    This is _not_ an open source project, but we made the source code available for
    viewing and contributions.

    GitLab Community Edition is merged daily to GitLab Enterprise Edition.

    GitLab EE requires a license key to be used.

gitlab-elasticsearch-indexer:
  name: GitLab Elasticsearch Indexer
  path: gitlab-org/gitlab-elasticsearch-indexer
  link: https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer
  description: |
    An Elasticsearch indexer for Git repositories. Used by GitLab EE to
    implement Global Code Search.

gitlab-shell:
  name: GitLab Shell
  path: gitlab-org/gitlab-shell
  link: https://gitlab.com/gitlab-org/gitlab-shell
  mirrors:
    - https://dev.gitlab.org/gitlab/gitlab-shell
    - https://github.com/gitlabhq/gitlab-shell
  description: |
    GitLab Shell handles Git commands for GitLab. It's an essential part of GitLab.

gitlab-workhorse:
  name: GitLab Workhorse
  path: gitlab-org/gitlab-workhorse
  link: https://gitlab.com/gitlab-org/gitlab-workhorse
  description: |
    Gitlab-workhorse is a smart reverse proxy for GitLab. It handles "large" HTTP
    requests such as file downloads, file uploads, Git push/pull and Git archive
    downloads.

omnibus-gitlab:
  name: Omnibus GitLab
  path: gitlab-org/omnibus-gitlab
  link: https://gitlab.com/gitlab-org/omnibus-gitlab
  mirrors:
  - https://dev.gitlab.org/gitlab/omnibus-gitlab
  - https://github.com/gitlabhq/omnibus-gitlab
  description: |
    Omnibus GitLab creates the packages for GitLab.

cookbook-omnibus-gitlab:
  name: Cookbook Omnibus GitLab
  path: gitlab-org/cookbook-omnibus-gitlab
  link: https://gitlab.com/gitlab-org/cookbook-omnibus-gitlab
  description: |
    Chef Cookbooks for deploying omnibus-gitlab package

gitlab-com-infrastructure:
  name: GitLab.com - infrastructure Terraform files
  path: gitlab-com/gitlab-com-infrastructure
  link: https://gitlab.com/gitlab-com/gitlab-com-infrastructure
  description: |
    Terraform - configuration and provisioning files for virtual machine nodes on production and staging

gitlab-com-runbooks:
  name: Gitlab.com - "on call" runbooks
  path: gitlab-com/runbooks
  link: https://gitlab.com/gitlab-com/runbooks
  description: |
    Describes system components, triage procedures, and commands to use in scenarios commonly faced
    by GitLab.com production support engineers

infrastructure:
  name: GitLab.com - infrastructure issue tracker
  path: gitlab-com/infrastructure
  link: https://gitlab.com/gitlab-com/infrastructure
  description: |
    Used to track the infrastructure work of GitLab.com itself

gitlab-cookbooks:
  name: GitLab.com - infrastructure node provisioning by role
  path: gitlab-cookbooks
  link: https://gitlab.com/gitlab-cookbooks
  description: |
    This is a group with a project - cookbooks - for each provisioned role in the GitLab.com cluster.
    These cooksbooks are applied after the virtual machine node is provisioned by Terraform project.

gitlab-cog:
  name: GitLab.com COGS
  path: gitlab-cog
  link: https://gitlab.com/gitlab-cog
  description: |
    This is a group which contains a separate project for each COG used by the GitLab.com infrastructure.

    A COG is an integration between the role of a node (or cluster) and a Chat application such as Slack.  The COG allows
    the infrastructure node (or cluster) to post status and alerts into Chat Channels and also allows for commands
    to be issued in the Chat Channel that control the behavior of the infrastructure node/cluster.

gitlab-development-kit:
  name: GitLab Development Kit
  path: gitlab-org/gitlab-development-kit
  link: https://gitlab.com/gitlab-org/gitlab-development-kit
  description: |
    GitLab Development Kit (GDK) provides a collection of scripts and other
    resources to install and manage a GitLab installation for development
    purposes. The source code of GitLab is spread over multiple repositories
    and it requires Ruby, Go, Postgres/MySQL, Redis and more to run.

    GDK helps you install and configure all these different components,
    and start/stop them when you work on GitLab.

gitaly:
  name: Gitaly
  path: gitlab-org/gitaly
  link: https://gitlab.com/gitlab-org/gitaly
  description: |
    Git RPC service for handling all the git calls made by GitLab.

gitlab-qa:
  name: GitLab QA
  path: gitlab-org/gitlab-qa
  link: https://gitlab.com/gitlab-org/gitlab-qa
  description: |
    End-to-end, black-box, entirely click-driven integration tests for GitLab.

www-gitlab-com:
  name: GitLab Inc. Homepage
  path: gitlab-com/www-gitlab-com
  link: https://gitlab.com/gitlab-com/www-gitlab-com
  description: GitLab Inc. Homepage available at about.GitLab.com

marketo-tools:
  name: Marketo Tools
  path: gitlab-com/marketo-tools
  link: https://gitlab.com/gitlab-com/marketo-tools
  description: Internal Marketo Tools

githost:
  name: GitHost.io
  path: gitlab-com/githost
  link: https://gitlab.com/gitlab-com/githost
  description: Hosted version of GitLab

gitlab-pages:
  name: GitLab Pages
  path: gitlab-org/gitlab-pages
  link: https://gitlab.com/gitlab-org/gitlab-pages
  description: |
    GitLab Pages daemon used to serve static websites for GitLab users

gitlab-runner:
  name: GitLab Runner
  path: gitlab-org/gitlab-ci-multi-runner
  link: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner
  description: GitLab CI/CD Runner

license-app:
  name: License App
  path: gitlab/license-app
  link: https://dev.gitlab.org/gitlab/license-app
  description: Internal GitLab License App

customers-app:
  name: Customers App (Subscription Portal)
  path: gitlab-come/customers-gitlab-com
  link: https://gitlab.com/gitlab-com/customers-gitlab-com
  description: Internal GitLab Customers App

gitlab-license:
  name: GitLab License
  path: gitlab/gitlab-license
  link: https://dev.gitlab.org/gitlab/gitlab-license
  description: Internal GitLab License Distribution App

university:
  name: GitLab University
  path: gitlab-org/university
  link: https://gitlab.com/gitlab-org/university
  description: Internal GitLab University

version-gitlab-com:
  name: version.gitlab.com
  path: gitlab/version-gitlab-com
  link: https://dev.gitlab.org/gitlab/version-gitlab-com
  description: Internal GitLab Version App

gitlab-contributors:
  name: GitLab Contributors
  path: gitlab-com/gitlab-contributors
  link: https://gitlab.com/gitlab-com/gitlab-contributors
  description: Application behind contributors.GitLab.com

gitlab-templates:
  name: GitLab CI/CD Templates
  path: gitlab-org/gitlab-ci-yml
  link: https://gitlab.com/gitlab-org/gitlab-ci-yml
  description: GitLab CI/CD Templates

pages-gitlab-io:
  name: pages.gitlab.io
  path: pages/pages.gitlab.io
  link: https://gitlab.com/pages/pages.gitlab.io
  description: GitLab Pages landing page

gitlab-anti-spam-toolkit:
  name: GitLab Anti-Spam Toolkit
  path: MrChrisW/gitlab-anti-spam-toolkit
  link: https://gitlab.com/MrChrisW/gitlab-anti-spam-toolkit
  description: GitLab Anti-Spam Toolkit

gitlab-monitor:
  name: GitLab Monitor
  path: gitlab-org/gitlab-monitor
  link: https://gitlab.com/gitlab-org/gitlab-monitor
  description: Tooling used to monitor GitLab.com

gitlab-docs:
  name: GitLab Docs
  path: gitlab-com/gitlab-docs
  link: https://gitlab.com/gitlab-com/gitlab-docs
  description: Project behind docs.GitLab.com

gitlab-markup:
  name: GitLab Markup
  path: gitlab-org/gitlab-markup
  link: https://gitlab.com/gitlab-org/gitlab-markup
  description: Markup render for non Markdown content

release-tools:
  name: GitLab Release Tools
  path: gitlab-org/release-tools
  link: https://gitlab.com/gitlab-org/release-tools
  description: Instructions and tools for releasing GitLab

takeoff:
  name: takeoff
  path: gitlab-org/takeoff
  link: https://gitlab.com/gitlab-org/takeoff
  description: Tooling used to deploy GitLab.com to any environment

spam-master:
  name: Spam-Master
  path: gitlab-com/spam-master
  link: https://gitlab.com/gitlab-com/spam-master
  description: |
    Collection of spam fighting API scripts for GitLab instances

rubocop-gitlab-security:
  name: RuboCop-GitLab-Security
  path: gitlab-org/rubocop-gitlab-security
  link: https://gitlab.com/gitlab-org/rubocop-gitlab-security
  description: |
    GitLab RuboCop gem for static analysis.

gitlab-design:
  name: GitLab Design
  path: gitlab-org/gitlab-design
  link: https://gitlab.com/gitlab-org/gitlab-design
  description:
    GitLab Design is used to jumpstart design work through the use of our design library.
    It is intended to enable frequent, stable, and consistent contributions while making
    GitLab's design open and transparent. This project helps facilitate design handoffs and
    design–development communication.

charts-gitlab-io:
  name: GitLab Helm Charts
  path: charts/charts.gitlab.io
  link: https://gitlab.com/charts/charts.gitlab.io
  description: |
    GitLab's official All-in-one Helm charts.

helm-gitlab-io:
  name: GitLab Cloud Native Helm Charts
  path: charts/helm.gitlab.io
  link: https://gitlab.com/charts/helm.gitlab.io
  description: |
    GitLab's official Cloud Native Helm charts.

kubernetes-gitlab-demo:
  name: Kubernetes GitLab Demo
  path: gitlab-org/kubernetes-gitlab-demo
  link: https://gitlab.com/gitlab-org/kubernetes-gitlab-demo
  description: |
    Deprecated GitLab Idea to Production Kubernetes demo project

functional-group-updates:
  name: Functional Group Updates
  path: gitlab-org/functional-group-updates
  link: https://gitlab.com/gitlab-org/functional-group-updates
  description: |
    Presentations from (some of) the teams at GitLab, to update the rest of the
    world on what they've been working on.
