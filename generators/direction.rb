require 'httparty'

PRIVATE_TOKEN = ENV['PRIVATE_TOKEN']

class GitLabInstance
  attr_reader :name, :endpoint

  def initialize(endpoint, private_token, name)
    @endpoint = "#{endpoint}/api/v4"
    @private_token = private_token
    @name = name
  end

  def call(path, params = '')
    url = @endpoint + path + params
    response = HTTParty.get(url, headers: { 'PRIVATE-TOKEN' => @private_token })

    puts "Error in retrieving URL #{url}: #{response.code}" if response.code != 200

    response
  end
end

class GitLabProject
  def initialize(id, instance)
    @id = id
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.call("/projects/#{@id}/milestones?state=active")
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end

  def milestone(milestone_id)
    @instance.call("/projects/#{@id}/milestones/#{milestone_id}")
  end

  def milestone_direction_issues(milestone_id)
    @instance.call("/projects/#{@id}/issues", "?milestone=#{milestone_id}&labels=direction")
  end

  def wishlist_issues(label, not_label = nil)
    result = @instance.call("/projects/#{@id}/issues", "?labels=direction,#{label}&state=opened&per_page=100&sort=asc")
    result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
    result.select { |issue| issue['milestone'].nil? || issue['milestone']['title'] == 'Backlog' }
  end

  def tier_issues(tier)
    @instance.call("/projects/#{@id}/issues", "?labels=direction,#{tier}&state=opened&per_page=100&sort=asc")
    # result = result.select { |issue| issue["milestone"] && issue["milestone"]["due_date"] }
  end

  def name
    project['name']
  end

  def web_url
    project['web_url']
  end

  def project
    @project ||= @instance.call("/projects/#{@id}")
  end

  def endpoint
    @instance.endpoint
  end
end

class GitLabGroup
  def initialize(id, instance)
    @id = id
    @instance = instance
    @name = instance.name
  end

  def milestones
    result = @instance.call("/groups/#{@id}/milestones?state=active")
    result = result.select { |ms| ms['due_date'] }
    result.sort_by! do |ms|
      Date.parse ms['due_date']
    end
  end
end

def issue_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << ' <kbd>EE Starter</kbd>' if issue['labels'].include? 'EE Starter'
  output << ' <kbd>EE Premium</kbd>' if issue['labels'].include? 'EE Premium'
  output << ' <kbd>EE Ultimate</kbd>' if issue['labels'].include? 'EE Ultimate'
  output << "\n"
  output
end

def tier_bullet(issue)
  output = "- [#{issue['title']}](#{issue['web_url']})"
  output << " <kbd>#{issue['milestone']['title']}</kbd>" if issue['milestone']
  output << "\n"
  output
end

def generate_direction
  print 'Generating direction...'
  direction_output = ''
  issues = []

  edition.each do |project|
    milestones = project.milestones + gitlaborg.milestones
    milestones.sort_by! do |ms|
      Date.parse ms['due_date']
    end

    milestones.each do |ms|
      if ms['due_date'] && Date.parse(ms['due_date']) >= Date.today
        issues += project.milestone_direction_issues(ms['title'])
      end
    end
  end
  issues.group_by { |issue| issue['milestone']['title'] }.each do |title, group|
    direction_output << "#### #{title} \n\n"

    group.each do |issue|
      direction_output << issue_bullet(issue)
    end

    direction_output << "\n"
  end
  print "\n"

  direction_output
end

def edition
  @edition ||= begin
                 com = GitLabInstance.new('https://gitlab.com', PRIVATE_TOKEN, 'GitLab.com')
                 ce = GitLabProject.new('gitlab-org%2Fgitlab-ce', com)
                 ee = GitLabProject.new('gitlab-org%2Fgitlab-ee', com)
                 omnibus = GitLabProject.new('gitlab-org%2Fomnibus-gitlab', com)
                 [ce, ee, omnibus]
               end
end

def gitlaborg
  @group ||= begin
               com = GitLabInstance.new('https://gitlab.com', PRIVATE_TOKEN, 'GitLab.com')
               GitLabGroup.new('gitlab-org', com)
             end
end

def label_list(label, not_label = nil, editions = nil)
  output = ''

  editions = edition if editions.nil?

  editions.each do |project|
    issues = project.wishlist_issues(label, not_label)
    issues.each do |issue|
      output << issue_bullet(issue)
    end
  end
  output = "No current issues\n" if output.empty?
  output
end

def tier_list(label)
  output = ''
  issues = []

  edition.each do |project|
    issues += project.tier_issues(label)
  end
  issues.sort_by! do |issue|
    if issue.dig('milestone', 'due_date')
      Date.parse(issue['milestone']['due_date'])
    else
      Date.new(2050, 1, 1)
    end
  end
  issues.each do |issue|
    output << tier_bullet(issue)
  end

  output = "No current issues\n" if output.empty?
  output
end

def generate_wishlist
  print 'Generating wishlist...'
  wishlist_output = {}

  ['chat commands', 'ci-build', 'code review', 'container registry', 'deploy', 'deliver', 'issue boards', 'issues', 'pages', 'pipeline', 'major wins', 'moderation', 'moonshots', 'open source', 'performance', 'Prometheus', 'service desk', 'test', 'usability', 'vcs for everything', 'wiki', 'HA', 'Cloud Native'].each do |label|
    wishlist_output[label] = label_list(label)
  end
  wishlist_output['CI/CD'] = label_list('CI/CD', ['ci-build', 'deploy', 'deliver', 'pages', 'pipeline', 'test', 'container registry', 'chat commands'])
  wishlist_output['build'] = label_list('direction', ['HA', 'Cloud Native'], Array(edition[2]))
  print "\n"

  ['EE Starter', 'EE Premium', 'EE Ultimate'].each do |tier|
    wishlist_output[tier] = tier_list(tier)
  end

  wishlist_output
end
