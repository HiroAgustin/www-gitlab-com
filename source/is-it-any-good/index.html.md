---
layout: markdown_page
title: Is it any good?
suppress_header: true
---

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab Has 2/3 Market Share in the Self-Hosted Git Market

With more than 100,000 organizations self-hosting GitLab, we have the largest share of companies who choose to host their own code. We’re estimated to have two-thirds of the single tenant market. When [Bitrise surveyed](http://blog.bitrise.io/2017/01/27/state-of-app-development-in-2016.html#self-hosted) ten thousand developers who build apps regularly on their platform, they found that 67 percent of self-hosted apps prefer GitLab’s on-premise solution.

![Image via Bitrise blog](/images/blogimages/bitrise-self-hosted-chart.png){: .shadow}<br>

Similarly, in their survey of roughly one thousand development teams, [BuddyBuild found](https://www.buddybuild.com/blog/source-code-hosting#selfhosted) that 79% of mobile developers who host their own code have chosen GitLab:

![Image via buddybuild blog](/images/blogimages/buddybuild-self-hosted-chart.png){: .shadow}<br>

In their articles, both Bitrise and BuddyBuild note that few organizations use self-hosted instances. We think there is a selection effect since both of them are SaaS-only offerings. Based on our experience, most large organizations (over 750 people) self host their Git server (frequently on a cloud service like AWS or GCP) than to use a SaaS service.

## GitLab CI Is the Most Popular Next-Generation CI System

Our commitment to seamless integration extends to CI. Integrated CI/CD is both more time and resource efficient than a set of distinct tools, and allows developers greater control over their build pipeline, so they can spot issues early and address them at a relatively low cost. Tighter integration between different stages of the development process makes it easier to cross-reference code, tests, and deployments while discussing them, allowing you to see the full context and iterate much more rapidly. We've heard from customers like [Ticketmaster](https://about.gitlab.com/2017/06/07/continous-integration-ticketmaster/) that adopting GitLab CI can transform the entire software development lifecycle (SDLC), in their case helping the Ticketmaster mobile development team deliver on the longstanding goal of weekly releases. As more and more companies look to embrace CI as part of their development methodology, having CI fully integrated into their overall SDLC solution will ensure these companies are able to realize the full potential of CI. You can read more about the benefits of integrated CI in our white paper, [Scaling Continuous Integration](http://get.gitlab.com/scaled-ci-cd/).

In his post on [building Heroku CI](https://blog.heroku.com/building-tools-for-developers-heroku-ci), Heroku’s Ike DeLorenzo noted that GitLab CI is “clearly the biggest mover in activity on Stack Overflow,” with more popularity than both Travis CI and Circle CI:

![Image via Heroku blog](/images/blogimages/heroku-questions-chart.png){: .shadow}<br>

While the use of Jenkins for CI is still higher than any other solution, we see more and more organizations moving from Jenkins, because upgrading their Jenkins server is a brittle process. The last two big things that GitLab CI lacked were scheduled builds (contributed to [GitLab 9.2](https://about.gitlab.com/2017/05/22/gitlab-9-2-released/)) and cross-project builds (released in [GitLab 9.3 on June 22](https://about.gitlab.com/2017/06/22/gitlab-9-3-released/)).

## GitLab CI is a leader in the The Forrester Wave™

![Forrester Wave graphic](/images/blogimages/forrester-ci-wave-graphic.png){: .shadow}

[ Forrester has evaluated GitLab as a Leader in Continuous Integration in The Forrester Wave™: Continuous Integration Tools, Q3 2017 report.](https://about.gitlab.com/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/)

## GitLab is one of the top 30 open source projects

[GitLab is one of the 30 Highest Velocity Open Source Projects](https://about.gitlab.com/2017/07/06/gitlab-top-30-highest-velocity-open-source/).

## GitLab has more than 1750 contributors

[GitLab contributors list](http://contributors.gitlab.com/).

## What is this page called 'is it any good?'?

When people first hear about a new product they frequently ask if it is any good. A Hacker News user [remarked](https://news.ycombinator.com/item?id=3067434): 'Note to self: Starting immediately, all raganwald projects will have a “Is it any good?” section in the readme, and the answer shall be “yes."'. We took inspiration from that and added it to the [GitLab readme](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/README.md#is-it-any-good). This page continues that tradition.
