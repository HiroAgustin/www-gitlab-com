---
layout: markdown_page
title: Courses
description: Learn about GitLab courses, including course listings and information for adding and uploading courses.
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Introduction
{: #introduction}

Welcome to the courses page! This is the central location to find course information, listings and links to content. If you are coming here to add a course please follow the instructions below.
For information about how to use Grovo please visit the [learning & development page](https://about.gitlab.com/handbook/people-operations/learning-and-development/index.html)

## Adding a Course
{: #adding-a-course}

If you have anything to share (no matter the quality level) please add it to this page by:

1. Making sure all the content is publicly viewable. Upload video's to our YouTube channel. If there is a presentation in Google Sheets make sure anyone can view it. If there is written content either add it to the relevant part of the handbook or create a page like https://about.gitlab.com/handbook/people-operations/courses/sls-101 and assign the merge request to Abby Matthews who will happily review and merge it for you.
1. Give the code a unique identifier in the form of AAA111, first three letters are for the department, numbers are unique and first number specifies the difficulty level of the course. As a guideline, in many university settings, there are only two levels, i.e. 1xx for "introductory" courses, and 2xx for more advanced courses.
1. Add the course to the bottom of this page. If you made a course list on another page (like university or support) you can use just one link to link to the entire list. If the courses are not in one list please link to each individually.
1. Optionally you can create a quiz in [Grovo](/source/handbook/people-operations/learning-and-development/index.html.md#frequently-asked-question-faqs).

Notes:

- We do not create custom course content in Grovo because everyone should be able to contribute to the courses. The courses are part of our handbook or documentation and versioned with git so people can contribute via merge requests. The exception to this are the the individual (IC) and manager (MGR) courses that consist of standard Grovo content.
- All videos are publicly listed on Youtube under our account so they are easy to discover and accessible from many different platforms.
- Please use Youtube instead of Google Drive that only be viewed by team members. YouTube's functionality is superior (watch later, caching around the world, support by mobile telephone providers, speed control). But allowing users (potential customers) and applicants (potential team members) to watch and sent it to other people also greatly increases the impact of them. If needed use the 'Organization code names' doc to obfuscate customer names.

## Getting Access to Youtube
{: #getting-access-to-youtube}

1. If you don't have access to the Youtube channel you can find the details in 1password in the Team Vault.
1. Type in Youtube, you should see a secure note with the information on how to get added to the account.

## Uploading Courses to Youtube
{: #uploading-courses-to-youtube}

 - To upload the video, go to YouTube and click the up arrow at the top right corner, next to the GitLab profile picture.
 - Change the security level from "public" to either "unlisted" (only those with the link can view) or "private" (only people with access to the GitLab YouTube account can view), so that you can edit the video prior to it being live.
 - Under "Basic Info", change the title to follow this pattern: INF 201 - Using Terraform to manage the GitLab.com infrastructure
 - After the video is done uploading, click "Video Manager" in the bottom right corner.
 - Edit the video to start when the training actually starts:
        - Click Edit next to the video icon.
        - Click the Enhancements tab on the top menu bar.
        - Click Trim on the bottom right. Slide the left edge of the bar to a few moments before the training begins, and the right edge of the bar to a few moments after the training ends. Click **Done**.
Take a screenshot of the second slide of the presentation (if applicable) to make it as custom thumbnail for your video on YouTube. You can upload your custom thumbnail under the Info and Settings tab when you are editing a video.
After the video is finished being edited, change the security level back to "public".


## Course Listings
{: #course-listings}

Please note that the courses under the Individual Contributor and Manager headings have been created using Grovo. You will need to be logged into Grovo to be taken straight to them when clicking on the links.

### Individual Contributor (IC) courses

- [IC 004 Social Media (28 mins)](https://app.grovo.com/training?aid=68573)
- [IC 120 Building Effective Communication Skills (13 mins)](https://app.grovo.com/training?aid=64043)
- [IC 130 Collaboration & Consensus (8 mins)](https://app.grovo.com/training?aid=64165)
- [IC 140 Productivity Under Pressure (10 mins)](https://app.grovo.com/training?aid=64140)
- [IC 141 Effective Productivity (30 mins)](https://app.grovo.com/training?aid=68383)
- [IC 143 How to Manage Projects (36 mins)](https://app.grovo.com/training?aid=68138)

### Manager (MGR) courses

- [MGR 100 The Role of a Manager (15 mins)](https://app.grovo.com/training?aid=64001)
- [MGR 101 Develop yourself as a Manager (1hr)](https://app.grovo.com/training?aid=69947)
- [MGR 120 IC Communicating Effectively (18 mins)](https://app.grovo.com/training?aid=64003)
- [MGR 140 Productivity Under Pressure (12 mins)](https://app.grovo.com/training?aid=64005)
- [MGR 160 Managing Performance Issues (12 mins)](https://app.grovo.com/training?aid=64006)
- [MGR 161 Develop Your Team (31 mins)](https://app.grovo.com/training?aid=68238)
- [MGR 162 Motivate & Enable Your Team (43 mins)](https://app.grovo.com/training?aid=68384)
- [MGR 165 Self Improvement & Team Dynamics (31 mins)](https://app.grovo.com/training?aid=69161)
- [MGR 170 Financial Fundamentals (18 mins no quiz)](https://app.grovo.com/training?aid=69160)
- [MGR 200 Strategic Management (35 mins)](https://app.grovo.com/training?aid=68572)
- [MGR 210 Fostering Creativity & Innovation (44 mins)](https://app.grovo.com/training?aid=68572)

### Leadership (LDR) courses

- LDR 101 [One-on-Ones: Guidance at GitLab video](https://www.youtube.com/watch?v=KUxxjGJv1dQ&t=12s) and [slide deck](https://docs.google.com/presentation/d/1h6KdMQFFWco3pSoCD3WrgIaxL06w6NCtkMg4ZYmAqBA/edit#slide=id.g1e97e26035_2_30)
- LDR 102 [Underperformance](https://www.youtube.com/watch?v=nRJHvzXwXBU&list=PLFGfElNsQthYYlad7vtTUt3wXKQUTsZWz&index=3) and [slide deck](https://docs.google.com/presentation/d/1kVbFPzuVpjXhHNG2po8-j3013RUEYN0jK4n8ecbCU9I/edit#slide=id.g1fa059f00f_0_0)
- LDR 103 [Career Mapping](https://www.youtube.com/watch?v=YoZH5Hhygc4) and [slide deck](https://docs.google.com/presentation/d/1zqfXI5WNwr8XsvtBusP9zG_gFYDE-WJZPEU4A_EcM7U/edit#slide=id.g24bdf5a984_1_56)
- LDR 104 [Promotions](https://www.youtube.com/watch?v=TNPLiYePJZ8&t=492s) and [slide deck](https://docs.google.com/presentation/d/1QvWSTO2NCfz8XBMwnUBElvx-d7zGIi3WIEl8L9DR4tg/edit#slide=id.g1fa059f00f_0_0)
- LDR 105 [How to Give Performance Review Feedback](https://youtu.be/VK8cA8nYcoY) and [slide deck](https://docs.google.com/presentation/d/1obMvPNicsQBB8_vQC1OaQnsNPLzKkoxvzxFoQCPjzuU/edit#slide=id.g1fa059f00f_0_0)
- LDR 106 [Decision Making](https://youtu.be/7RbVQwU69H0) and [slide deck](https://docs.google.com/presentation/d/1ua-EShTeT1PD23Yw-5C2OTnqy8x58nR--b6QeRKeToI/edit#slide=id.g1fa059f00f_0_0)

### University (UNI) courses

- TODO Code all courses on [https://docs.gitlab.com/ce/university/](https://docs.gitlab.com/ce/university/) and add a single link from here (instead of listing all courses which would lead to duplication).

### Sales (SLS) courses

 - SLS001 [How to Conduct an Executive Meeting](https://www.youtube.com/watch?v=PSGMyoEFuMY&feature=youtu.be)

### Customer Success (CST) courses

**Office Hours**

- Coming Soon!

**Explainer Videos**

- CST201 [GitHub.com vs. GitLab](https://www.youtube.com/watch?v=ZdmDJFPNQuI&index=1&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST202 [Hosting Gitlab in the Cloud](https://www.youtube.com/watch?v=1OLCDtUkw0Q&index=2&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST203 [Integrating Atlassian and GitLab](https://www.youtube.com/watch?v=o7pnh9tY5LY&index=3&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST204 [GitLab Maintenance and Support](https://www.youtube.com/watch?v=X8jsj59b4vk&index=4&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST205 [We already use JIRA, so shouldn't we go with BitBucket?](https://www.youtube.com/watch?v=-JRab22h9Dg&index=5&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)

### Finance (FIN) courses

- TODO

### Build (BLD) courses

- BLD001 [GitLab Pivotal Cloud Foundry Tile](https://drive.google.com/open?id=0B5OISI5eJZ-DVlEzWUstSF85MHc)
- BLD002 [GitLab Terraform Module](https://drive.google.com/open?id=0B0iWGgM5SiBGMjNfQ3BCbFl0Ums)

### Engineering (DEV) courses

- [DEV 101 - Contributing to Golang projects](/courses/dev-101)

### Backend (BCK) courses

 - TODO

### Infrastructure (INF) courses

- [INF 201 - Using Terraform to manage the GitLab.com infrastructure](/courses/inf-201)
