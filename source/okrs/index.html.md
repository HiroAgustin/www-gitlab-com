---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](https://about.gitlab.com/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals

## Format

Before the quarter:

`Owner: Key Result as a sentence. Metric`

During and after the quarter:

`Owner: Key Result as a sentence. Metric => Result`

- Owner is the title of person that will own the result.
- We use two spaces to indent instead of tabs.
- OKRs start with the owner of the key result. When referring to a team lead we don't write 'lead' since it is shorter and the team goal is the same.
- The key result can link to an issue.
- The metric can link to real time data about the current state.
- The three company/CEO objectives are level 3 headers to provide some visual separation.

## Levels

We only list your key results, these have your (team) name on them.
Your objectives are the key results under which your key results are nested, these should normally be owned by your manager.
We do OKRs up to the team or director level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Part of the individual performance review is the answer to: how much did this person contribute to the team key objects?
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the team level we end up with a maximum 4 layers of indentation on this page (this layer count excludes the three company/CEO objectives).
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every team should have at most 9 key results. To make counting easier only mention the team and people when they are the owner.
The advantage of this format is that the OKRs of the whole company will fit on 3 pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #okrs channel and at-mention the CEO.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Critical acclaim

Spontaneous chat messages from team members after introducing this:

- As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up
- I like it too, especially the fact that it is in one page, and that it stops at the team level.
- I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.
- I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."
- I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## 2017-Q3

Summary: With a great team make a popular next generation product that grows revenue.

### Objective 1: Grow incremental ACV according to plan.

* CEO: Build [sales qualified leads (SQL)](/handbook/marketing/business-development/#sql) pipeline for new business. 300% of [incremental annual contract value (IACV)](/handbook/finance/#iacv) plan
  * CMO: Identify CE installations within strategic organizations with installations with over 500 users and launch demand generation campaigns.
  * CMO: More demand coming from inbound marketing activities. 50% of sales pipeline quota
    * CMO: Increase EE Trials. 21% Quarter over Quarter (QoQ)
    * CMO: Increase EE Trial SQL $ pipeline contribution 43% QoQ
  * CFO: Forecast accuracy and delivery of relevant information
    * Controller: Zuora to Salesforce reconciliation complete and tested. DONE
    * CFO: Forecasting model projects forecast +/- 10% normalized for big deals. DONE
    * Controller: Zuora to Salesforce to Zendesk data synch completed. DONE
  * CFO: Deliver 10 SQLs > $10k during quarter through investor outreach 50% ACHIEVED
  * VP Prod: Salesforce shows usage. [Version check](https://gitlab.com/gitlab-com/salesforce/issues/104), usage ping, and ConvDev index are available in SFDC. => All shipped
  * VP Prod: Import documentation for all major platforms. Five new import docs shipped. => 5/5 shipped. 10 in total now.
  * CRO: 100% of quota-carrying reps at 3x pipeline of quarterly IACV goal
    * Alliances: generates 10% of sales pipeline
    * Federal: generates 10% of sales pipeline
    * AE/Customer Success: [generate 15% of sales pipeline quota](https://na34.salesforce.com/00O61000003npUd)
    * SDR lead: Generate 25% of sales pipeline quota
      * SDR lead: Get an SQL from Fortune 1000 companies identified using CE. 50% of identified companies using CE.
      * SDR lead: Double Win Rate. 10%
  * CRO: Increase new business sales velocity. Grow 200% YoY (Q3 2016 to Q3 2017) in new business deals closed won.
  * CRO: Decrease age of non-web direct deals by 10%
    * Sales Ops: [New sales stages and exit criteria. Launch and Train](https://gitlab.com/gitlab-com/sales/issues/120)
    * Customer Success: Solution Architects managing PoC's for new business opps within strategic/large accounts. 50%
    * PMM: 2 ROI calculators
    * PMM: 3 case studies
  * CRO: Keep sales efficient. [Sales efficiency ratio](/handbook/finance/#definitions) > 1.8 (IACV / sales spend)
  * CMO: Keep marketing efficient. [Marketing efficiency ratio](/handbook/finance/#definitions) > 3.4 (IACV / marketing spend)
* CEO: Understand and improve existing account growth. 200% YoY
  * VP Eng: Geo DR successful deployments. [Test fail-over with GitLab.com](https://gitlab.com/gitlab-org/gitlab-ee/issues/1884) => 20% done. Some customers running Geo in alpha now. Failover targeted for Q1 2018.
    * Platform: Get Geo reliable with a testing framework. => 10% done, moved to Q4 with GitLab QA
    * Build: Deliver PostgreSQL High Availability (HA) in omnibus and Terraform to enable HA
  * VP Prod: [Make it easier to discover and use EE features](https://gitlab.com/gitlab-org/gitlab-ee/issues/2417). Ship EE by default. Trials increase 100%. => Shipped. Trials  % TODO
    * Technical Writing: CE docs link to EE wherever relevant. Increased visits to EE docs. => Done. Defaults to EE docs now (i.e. over 100% increase)
  * VP Prod: Ship paid subscriptions for GitLab.com. All EE features are behind subscription. Ability to buy higher storage limits. => Done. Storage limits deprioritized.
  * Head Prod: [Improved support for Java development lifecycle](https://gitlab.com/gitlab-org/gitlab-ce/issues/33943). 2 projects done.
  * CFO: Make our sales process data driven
    * CFO: Lead hired DONE
    * Data and Analytics Architect: Data and Analytics vision and plan signed off by executive team NOT STARTED
    * Data and Analytics Architect: Create a user journey funnel NOT STARTED
  * CMO: Educational Email campaign series to educate users on full solution capabilities and how to get started. TODO # of campaign conversions
  * Customer Success: [License utilization for large and strategic accounts at a 90% average](https://gitlab.com/gitlab-com/customer-success/issues/79)
  * Customer Success: [Double spend for scheduled renewals valued at $20,000+](https://gitlab.com/gitlab-com/customer-success/issues/66)
  * Customer Success: [Double spend within large/strategic account segmentation](https://gitlab.com/gitlab-com/customer-success/issues/78)
  * Customer Success: Identity the trigger(s) to purchase for large/strategic accounts.
  * Support: Provide faster, more knowledgable support. Reduce Average Time to Solve by 5%.
* CEO: Increase [average sales price (ASP)](/handbook/finance/#definitions) (IACV per won) by 25% QoQ
    * CRO: Triple sales assisted average new business deal YoY
    * CRO: [50% of IACV from EEP](https://gitlab.com/gitlab-com/sales/issues/143)
    * CMO: More sales assets. 3 new for each stage ([TOFU, MOFU, BOFU](/handbook/glossary/#funnel)) of the sales process
      * CMO: [Presentation generation based on conversational development index.](https://gitlab.com/gitlab-com/organization/issues/95)
      * CMO: 3 minute video product demo targeted at buyer audience. Published
    * PMM: Sales Demo easy to give. At least one Account Executive can do it in 10 minutes.
    * CRO: Sales Pitch Deck and Messaging trained. 75% of customer facing team passes test.
    * VP Prod: Conversational Development Index in product and SFDC. => Done
    * VP Prod: Improve JIRA support. Better than Bitbucket. Support transitions, references and development panel. => Done: dev panel, references. Not possible yet: transitions.
      * Technical writing: Create a page on our JIRA integration and how to move to Issues. 100% increased usage of JIRA integration. => Done. % TODO

### Objective 2: Popular next generation product.

* CEO: GitLab.com ready for mission-critical tasks. 99% of user requests have [first byte](/handbook/engineering/performance/#first-byte) < 1s
  * VP Scaling: Highly Available. [99.9%](http://stats.pingdom.com/81vpf8jyr1h9/1902794/history)
    * Production: Scalable and HA setup for cache and background jobs storage.
    Persistence of cache (Redis) and background job storage (Sidekiq) split and
    HA is setup for persistent storage. => Done.
    * Production: Robust backups with automatic periodic restores. Counter on
    monitor.gitlab.net shows days since last automatic restore of database and file
    system. => Backups are tested but restores are not automatic, and counter is missing.
    * Production: Multi-canary deployments enabled. PoC of two canaries via kubernetes with Build team's Helm charts. => One canary available, not via kubernetes.
    * Production: Enable GitLab.com search using ElasticSearch. Provide a cluster that is 99% available for > 2 months in Q3. => Infrastructure available but application requires further work.
    * Database: Run pgbouncer from Omnibus. Shipped. => Done
    * Database: Use Omnibus provided software for HA / failover instead of using a GitLab.com specific setup. Shipped and In Use. => Not yet in use.
  * VP Eng: Enable High Availability
    * Platform: Enable graceful degradation when file servers are down. => Circuitbreakers built but not yet fully deployed.
    * Platform: Support multiple Redis clusters for persistent and cache stores. => Done.
    * Platform: Use Geo DR to move between clouds and Area Zones => 10% done, begin testing with Geo testbed, pushing to Q4.
    * Support: Improve GitLab provided debugging tools. We'll log all times we need terminal access and create an issue for each to improve GitLab tools. => 50% done, see https://gitlab.com/gitlab-com/support/issues/754
    * Prometheus: Reach parity with Prometheus metrics for Unicorn, Sidekiq, and gitlab-shell. Deprecate InfluxDB. => 30% done with Prometheus metrics for Unicorn. Sidekiq and gitlab-shell remain for Q4.
    * Edge: Make GitLab QA test [backup/restore](https://gitlab.com/gitlab-org/gitlab-qa/issues/22), [LDAP](https://gitlab.com/gitlab-org/gitlab-qa/issues/3), [Container Registry](https://gitlab.com/gitlab-org/gitlab-qa/issues/49), and [Mattermost](https://gitlab.com/gitlab-org/gitlab-qa/issues/26) => 25% done with Mattermost. Backup/restore + LDAP targeted for Q4.
    * CI/CD: Decrease monthly per minute cost of shared runners. Cost down by 30%. => 25% done, Google Compute Engine support implemented but not deployed.
    * CI/CD: Improve responsiveness for pipelines without any existing builds. 99% start within 1s.
  * VP Scaling: [Lower latency](https://gitlab.com/gitlab-com/infrastructure/issues/2373). 99% of user requests have [first byte](/handbook/engineering/performance/#first-byte) < [1s](https://performance.gitlab.net/dashboard/db/transaction-overview?panelId=2&fullscreen&orgId=1)
    * Gitaly: Gitaly service active on file-servers. => Done.
    * Gitaly: Roll out Gitaly migrations. 24 additional endpoints migrated to Gitaly and [in acceptance testing](https://gitlab.com/gitlab-org/gitaly/blob/master/README.md#current-features). => Done.
    * Gitaly: Reduce “idea to production” time of migrations. 80% of all migrations started in Q3 are enabled on GitLab.com within two GitLab releases. => Done by changing process.
    * Database: Reduce the p99 of [SQL timings](https://performance.gitlab.net/dashboard/db/daily-overview?panelId=12&fullscreen&orgId=1) across the board to 200 ms (100-200 ms less than what we have now). => Significant progress, but not all p99 < 200 ms. See [gitlab-ce#34535](https://gitlab.com/gitlab-org/gitlab-ce/issues/34535) for more context.
    * Production: Provide reliable [internal & external](https://about.gitlab.com/handbook/engineering/performance/#standards-we-use-to-measure-performance)
    baseline monitoring of overall service health. Implement baseline end to end
    monitoring for GitLab.com (api/web/git(ssh/https)) and define
    [SLO’s](https://about.gitlab.com/handbook/infrastructure/production-architecture/#infra-services)
    based on this baseline. => Baseline not defined.
    * Production: Solve performance issues. Implement [CDN for GitLab.com](https://gitlab.com/gitlab-com/infrastructure/issues/2092). => CDN was implemented but had to be rolled back.
  * VP Eng: Lower latency in application
    * Discussion: Solve performance issues. Reduce p95 of [discussion-related actions](https://performance.gitlab.net/dashboard/db/daily-overview?orgId=1) with over 10 hits/day to < 1 s. Reduce p99 to < 3 s. => 25% done. 10% (~150 of 1476 endpoints) still exceed thresholds. http://stats.gitlab.com/1902794 is at 1.2 s.
    * Platform: Solve performance issues. Reduce p95 of [platform-related actions](https://performance.gitlab.net/dashboard/db/daily-overview?orgId=1) with over 10 hits/day to < 1 s. Reduce p99 to < 3 s. 25% done. 10% (~150 of 1476 endpoints) still exceed thresholds.
    * Frontend: Do a [Manual performance audit](https://gitlab.com/gitlab-org/gitlab-ce/issues/33958) and deploy improvements. Implement Top 3 actions => Done
    * Frontend: Package optimization and [CDN Hosting for .com](https://gitlab.com/gitlab-com/infrastructure/issues/2092) => Done
    * Edge: [Ship large database seeder for developers](https://gitlab.com/gitlab-org/gitlab-ce/issues/28149). => 25% done
    * Edge: [Enable Bullet by default on the CI](https://gitlab.com/gitlab-org/gitlab-ce/issues/30129). => May not be practical given https://gitlab.com/gitlab-org/gitlab-ce/issues/30129#note_35800389
  * VP Eng: Eliminate critical stability issues
    * Discussion: Merge requests get merged 100% without ever getting into stuck locked state => 20% done. Improvements made in https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13207, but still occuring. We need to measure rate.
    * Platform: Project authorizations fast (< 1 s) and consistent (requires no manual refreshes). DONE
    * Platform: Project imports and forks complete 100% without ever getting stuck => 10% done. Import issues need to be broken out separately.
    * Platform: Namespace and project renames work 100% of the time => 30% done. Requires hashed storage support
    * Platform: Repository cache state eventually consistent within minutes (no manual expiration needed) => In progress with https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14980
    * Frontend: [Implement cross-browser automated testing](https://gitlab.com/gitlab-org/gitlab-ce/issues/6065). Catch at least one browser regression before release date. => 95% Done Not automated. Has to be turned on.
    * Frontend: Accomplish at least [3 frontend improvement issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name%5B%5D=frontend&milestone_title=Backlog) per release cycle. => 60% Done
    * CI/CD: Make [runners work on Google Compute Engine without dying halfway](https://gitlab.com/gitlab-com/infrastructure/issues/1936). Done
    * CI/CD: Track and ensure the number of job failures due to system failure. Number < 0.01%
  * VP Scaling: Secure platform
    * Security: Improve defenses. Implement top 10 actions from Risk Assessment. => Implemented top 12.
    * Security: Vulnerability testing. Conduct external testing. => Done.
    * Security: Improve security practices through people and processes. Backlog of security issues reduced by 50% (currently 148 issues in [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security), 9 in [gitlab-ee](https://gitlab.com/gitlab-org/gitlab-ee/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security), 69 on [infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security)). => Not done, backlog is larger.
* CEO: Increased usage of idea to production. 100% growth
  * CMO: Generate more company and product awareness including [overtake BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab).
  * VP Prod: Issue boards usage increase. 100% (.com and usage ping) => ~100% increase
  * VP Prod: Service Desk usage increase. 100% (.com and usage ping) => ~1000-2000% increase
  * VP Prod: Subgroups usage increase. 100% (.com and usage ping)
  * Head Prod: Monitoring usage increase. 100% (.com and usage ping)
    * Prometheus: [Increase adoption](https://gitlab.com/gitlab-org/gitlab-ce/issues/33556). 3 issues shipped.
  * Head Prod: Pipelines usage increase. 100% (.com and usage ping)
    * CI/CD: [Improve onboarding](https://gitlab.com/gitlab-org/gitlab-ce/issues/32638). 10 issues shipped.
  * Head Prod: Environments usage increase. 100% (.com and usage ping)
  * Head Prod: Review apps usage increase. 100% (.com and usage ping)
  * CTO: [Make autodeploy more practical](https://gitlab.com/gitlab-org/gitlab-ce/issues/33707). Undefined yet.
  * CTO: [Zero-configuration CI](https://gitlab.com/gitlab-org/gitlab-ce/issues/26941). Make it work for 5 popular frameworks.
  * CTO: [Lead update to Rails 5.1](https://gitlab.com/gitlab-org/gitlab-ce/issues/14286). Shipped.
  * CMO: Better explain our solution, features and value proposition. Flow page that links to feature pages.
  * UX: [Measure usability of critical user flows to identify areas needing improvement.](https://gitlab.com/gitlab-org/ux-research/issues/13) => 75% Research done, issues opened. Implementation and re-measure not done.
    * Propose optimizations of critical user flows based on results found in research.
    * Implement optimization and re-measure to ensure the user experience has improved.
  * Frontend: [Optimize Frontend Code](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=frontend&label_name[]=performance) to support speed. For larger projects and ones with thousands of comments. => Done a lot of Performance related optimisations, ongoing task
  * Build: Simplify HTTPS configuration. In Omnibus and Helm. For Rails app, container registry, and pages. [Consider Let's Encrypt](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1096).
  * Dir Partnerships: First team of OSS projects starts using GitLab for proof-of-concept/testing at Drupal, Gnome and Kubernetes.
  * Dir Partnerships: Get major partner to use it for CI.
  * Dir Partnerships: [AWS QuickStart guide](https://gitlab.com/gitlab-org/gitlab-ce/issues/29199) published
  * CMO: More contributors from the wider community each month. Unique contributors grow 10% QoQ
  * CMO: Initiate AR engagement with key analysts and achieve Leader in the Forrester Wave CI research report.
  * Head Prod: [Make sure the installation process is great](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2463). Complete all items.
  * VP Eng: Make sure existing features are used by GitLab.com. Cycle time analytics working.
    * Support: GitLab.com uses Service Desk. In use for one process.
    * Build: Deliver service specific Docker images and Helm charts usable for GitLab.com and our users. Rails, Workhorse, Pages, Registry, Gitaly, Shell.
    * CI/CD: Help GitLab.com use our deployment features. CD/Kubernetes/Helm/Canary deploys/Review Apps/Service Desk
    * Edge: [GDK based on Kubernetes](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/243) (e.g. minikube).
    * Edge: [CE Pipelines run in 30 minutes](https://gitlab.com/gitlab-org/gitlab-ce/issues/24899).
    * Edge: [Flaky tests don't break `master`](https://gitlab.com/gitlab-org/gitlab-ce/issues/32308).
    * Edge: [CE is automatically merged to EE daily](https://gitlab.com/gitlab-org/gitlab-ce/issues/25870).
    * Edge: [Triage policies](https://about.gitlab.com/handbook/engineering/issues/issue-triage-policies/) are [automatically enforced](https://gitlab.com/gitlab-issue-triage/issue-triage).
    * UX: [Make it easier to find and use advanced GitLab features](https://gitlab.com/gitlab-org/gitlab-ce/issues/25341) => 25% Implemented https://gitlab.com/gitlab-org/gitlab-ee/issues/3054 which was then reverted. Successfully implemented https://gitlab.com/gitlab-org/gitlab-ee/issues/3049
    * Frontend: [Support > 2000](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12069) comments/code changes on issues, diffs, and merge requests. With speed and without [jank](http://jankfree.org/). => 60% done. Needs work on MR discussion. 
    * Frontend: Reduce frontend render times of comments and diffs with a goal of %70 faster. => https://gitlab.com/gitlab-org/gitlab-ce/issues/4058 Speed Index Before 8081, Now 2757 (65% faster)
  * Head Prod: [Ensure all idea to production features are usable by GitLab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues/35021)
* CEO: Next generation product. Move vision forward every release.
  * Head Prod: Create and radiate vision for GitLab DevOps. [Publish vision video](https://gitlab.com/gitlab-org/gitlab-ce/issues/32640).
  * Head Prod: [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517). Ship kernel of Auto DevOps.
  * UX: [Improved navigation. Iterate on it every month.](https://gitlab.com/gitlab-org/gitlab-ce/issues/32794) => 100% All work completed, work continues
  * UX: [Improve perceived performance](https://gitlab.com/gitlab-org/gitlab-ce/issues/29666) => 0% Issues continually re-scheduled (UX Ready)
  * Frontend: [Instant user feedback](https://gitlab.com/gitlab-org/gitlab-ce/issues/27614) => Done
  * Frontend: [Repo as editor](https://gitlab.com/gitlab-org/gitlab-ce/issues/31890) => PoC Done, move to Beta as a stable feature in progress
  * Frontend: Get a [moonshot](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=moonshots) (e.g. VSCode/Monaco editor, Team dashboards) from PoC to Feature - Image Comments released + Multi file editor Beta in 10.3

### Objective 3: Great team.

* CEO: Effective leadership. NPS of leaders as rated by their reports 10% improvement. Update: This will be pushed back until [360 reviews](https://about.gitlab.com/handbook/people-operations/performance-reviews/) are established in 2018.
  * VP PO: Management completed basic management (MGR) classes. 75% completed
  * VP PO: 5 GitLab specific courses for the management team.  Published and delivered.
  * CFO: [Real-time analytics platform](https://about.gitlab.com/handbook/finance/analytics/). 40% of metrics live 50% ACHIEVED - 20% OF METRICS
* CEO: Attract great people. New hire score 2% improvement
  * VP PO: More sourced recruiting. 20% of total hires
  * VP PO: Hires from lower cost locations. 20% increase
  * VP PO: Interviewer rating (ELO). Implemented
  * VP PO: Quicker hiring cycle (plan interviews upfront). Hiring time -25%
  * VP PO: Improve our global compensation framework. Structured on better data.
* CEO: Retain great people. eNPS 0% change
  * Customer Success: Hiring talent to plan and ramping up SA’s so that on day 31 they are certified and ready to be assigned to accounts.
  * VP PO: Improve diversity. Two initiatives deployed.
  * VP PO: Train all Senior level and above team members on [how to interview](https://gitlab.com/gitlab-com/peopleops/issues/305).
  * VP PO: Develop and document benefits plans (current and considering) for each entity.

## 2017-Q4 DRAFT

### Objective 1: Grow incremental ACV according to plan.

* CRO: Sales efficiency ratio > 1.2 (IACV / sales+marketing)
  * CRO: Field Sales efficiency ratio > 1.8 (IACV / sales spend)
  * CMO: Marketing efficiency ratio > 3.4 (IACV / marketing spend)
* CMO: Establish credibility and thought leadership with Enterprise Buyers delivering on pipeline generation plan through the development and activation of integrated marketing and sales development campaigns:
  * CMO: Define category strategy, positioning and messaging and mobilize the company around the strategy.
    * CMO: Develop and document messaging framework
    * CMO: Develop and roll out updated pitch and analyst decks
  * CMO: Implement website redesign to support our awareness and lead generation objectives, accounting for distinct audiences.
  * MSD: achieve target in new inbound opportunity SQL $
  * MSD: achieve target in new outbound opportunity SQL $
  * MSD: achieve new opportunity volume target in strategic Sales segment accounts
  * CMO: Build out Product Marketing function, including hiring and onboarding three people, updating objectives, process and handbook, and developing cross-functional alignment.
    * PMM: Evolve and deliver updated AE pitch deck messaging and incorporate Sales feedback
    * PMM: Enhance ROI section of website including adding interstitial to promote individual calculators
    * PMM: Work with content team to identify 2 customers and deliver customer case studies and customer based web content
    * PMM: Evolve EEP vs EES vs CE differentiation messaging and optimize website experience for product sections
* CRO: 100% of new business IACV plan
  * CRO: Sales Health 80% of Reps on Quota
  * CRO: [Increase Average Sales Assisted New Business Deal by 30%](https://na34.salesforce.com/00O61000003jNT6)
    *  RD: 70% of sales assisted deals are EEP
    *  CS: Launch Professional Services - 25% of all deals with subscription ACV of $100,000+ include PS
  * CRO: Decrease Time to close (New Business - Sales Assisted) by 10%
    * Sales Ops: Launch MEDDPIC. Fields 100% filled out for all stage 3+ deals
* CRO: 165% net retention in [Q4 renewal value](https://na34.salesforce.com/00O61000001uN0a)
  * Sales Ops: 95% of subscriptions under $1200 moved to auto-renew
  * CS: 25% of large/strategic accounts on EES trialing EEP with how we expand game plan documented in SFDC
* CFO: Efficiency
  * Legal: Publish sales training guide on key legal terms common in deals
  * Legal: Hold one sales team training session published on courses
  * Legal: Implement a legal section for sales team onboarding
  * Legal: Use GitLab issues to track and collaborate legal matters
  * Controller: Plan for implementing ASC 606 approved
  * Controller: Move billing administration from SMB team to billing specialist
  * Billing Specialist: Create a Zuora training module for quick and efficient training of new sales reps.
  * Analytics: Create a user journey funnel

### Objective 2: Popular next generation product.

* CEO: Next generation
  * VP Product
    * Platform: Make way for a cloud IDE. Multi-file editor with terminal shipped.
    * Discussion: Portfolio management: Epics and roadmap view shipped (as part of EEU)
    * CI/CD: Improve support for Java development lifecycle. 1 more project done.
    * CI/CD: Improve existing features with additional value. 1 feature extended.
    * Prometheus: Make GitLab easier to monitor. [GA Prometheus monitoring of the GitLab server](https://gitlab.com/gitlab-org/gitlab-ce/issues/38568), deprecate InfluxDB.
    * Prometheus: Shift performance monitoring earlier in the pipeline, [detecting regressions prior to merge](https://gitlab.com/gitlab-org/gitlab-ee/issues/3173). Deliver one feature.
    * Prometheus: Complete the feedback loop by [comparing Canary and Stable performance](https://gitlab.com/gitlab-org/gitlab-ee/issues/2594).
  * VP Eng
    * Partner with PM to generate and implement process to measure and speed up development velocity
  * Support Lead
    * 85% Premium Support SLA (up from 68% last quarter)
    * +5% MoM on all other SLAs
    * Identify and close 100 tickets of debt in backlog
  * Design Lead
    * Document Gitlab UX standards / style guide and communicate  to internal team via UX FGU, the UX Style guide, and within issues
    * Finish Phase 1 of a design library of assets
  * Frontend (AC) Lead
    * Write 60 unit tests to resolve test debt in Q4 (evenly distributed throughout the team)
    * Crush 140 bugs this quarter (evenly distributed through the team)
    * Improve codebase by making modules ready for webpack by moving it to our new coding standards ([#38869](https://gitlab.com/gitlab-org/gitlab-ce/issues/38869))
    * Improve performance by making library updates and webpack bundle optimizations ([#39072](https://gitlab.com/gitlab-org/gitlab-ce/issues/39072))
    * Finish conversion from inline icons to SVG Icons to improve performance
  * Frontend (DC) Lead
    * Write 60 unit tests to resolve test debt in Q4 (evenly distributed throughout the team)
    * Crush 140 bugs this quarter (evenly distributed through the team)
    * Refactor the MR discussion in Vue to decrease load times, and increase performance/usability
  * Director of Backend
    * Author a demo script for use throughout the quarter
    * Expose EE and CE code coverage metrics
  * Build Lead
    * Establish baseline metric for install time/ease and come up with a plan to achieve and maintain it
    * Decrease build times from 60 minutes to 30 minutes
    * Create integration test for Mattermost
  * Platform Lead
    * Identify 1 sub-standard area of the code base and raise local unit test coverage up to project level
    * Write integration test for backup/restore
    * Make GitLab QA [test LDAP](https://gitlab.com/gitlab-org/gitlab-qa/issues/3)
    * Resolve or schedule all priority 1 & 2 Platform issues (and groom performance issues)
  * CI/CD Lead
    * Add 1 integration for runners
    * Resolve or schedule all priority 1 & 2 CI/CD issues (and groom performance issues)
    * Reduce amount of system failures to less than 0.1%
    * Improve cost efficiency of CI jobs processing for GitLab.com and GitLab Inc.
  * Discussion Lead
    * Write integration test for squash/rebase
    * Write integration test for protected branches
    * Resolve or schedule all Priority 1 & 2 Discussion issues (and groom performance issues)
  * Prometheus Lead
    * Reach parity with Prometheus metrics for Unicorn, Sidekiq, and gitlab-shell and Deprecate InfluxDB
    * Make Grafana dashboards available for all Prometheus data easy to install for GitLab instances
    * Identify 1 sub-standard area of the code base and raise local unit test coverage up to project level
    * Resolve or schedule all Priority 1 & 2 Prometheus issues (and groom performance issues)
  * Geo Team
    * Make Geo Generally Available
    * Geo performant at GitLab.com scale
    * Manual failover robust in Geo as first step to Disaster Recovery
  * Director of Quality
    * Document what quality means at GitLab on an about page
    * Communicate standard in 3 different ways to internal team
    * Make issue/board scheme change recommendation to allow us to better mine backlog for quality metrics
    * Initiate a project to make quality metrics and charts self-service
    * Initiate a project to allow for UI testing of the web application locally and on CI
  * Edge Lead
    * Ship [large database seeder for developers](https://gitlab.com/gitlab-org/gitlab-ce/issues/28149)
    * Enable [triage](https://gitlab.com/gitlab-org/triage) to be [used for any project](https://gitlab.com/gitlab-org/triage/issues/14#note_41293856)
    * Make GitLab QA [test the Container Registry](https://gitlab.com/gitlab-org/gitlab-qa/issues/49)
    * Make GitLab QA [test upgrade from CE to EE](https://gitlab.com/gitlab-org/gitlab-qa/issues/64)
    * Make GitLab QA [test simple push with PostReceive](https://gitlab.com/gitlab-org/gitlab-qa/issues/53)
    * [De-duplicate at least 5 redundant (feature) tests](https://gitlab.com/gitlab-org/gitlab-ce/issues/39829)
    * Improve at least the [5 longest spec files](https://redash.gitlab.com/queries/15) by at least 30%
    * Investigate code with less than 60% tests coverage and add tests for  at least the [5 most critical files](https://gitlab.com/gitlab-org/gitlab-ce/issues/19412)
    * [Investigate encapsulating instance variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/20045) about the current page in a class
    * [Reduce duplication](https://gitlab.com/gitlab-org/gitlab-ce/issues/31574) in at least 5 forms
    * Solve at least 3 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&milestone_title=No+Milestone)
  * Director of Security
    * Security Product. Document product vision and roadmap to MVP.
    * Compliance framework. Plan and first implementation.
  * CTO: [Scan source code for security issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/38413 ). Make it work for 3 popular frameworks.
  * CTO: [Less effort to merge CE into EE](https://gitlab.com/gitlab-org/gitlab-ee/issues/2952#note_41016654). 10 times less efforts to merge CE to EE
  * CTO: Investigate our options around chat functionality. Come up with a plan to have chat per project/group/instance.
* CEO: Partnerships
  * VP Product
    * Increase adoption of Kubernetes through integration.
    * CI/CD: Configure review apps and deployment for projects in less than 5 steps.
    * Prometheus: Enable [Prometheus monitoring of Kubernetes clusters]((https://gitlab.com/gitlab-org/gitlab-ce/issues/28916)) with a single click.
    * Platform: Help partners and customers adopt GitLab. Ship authentication and integration requirements.
    * Platform: Ship the GNOME requirements. 5 requirements shipped.
    * At least 3 GNOME projects migrated to GitLab as part of evaluation
    * [AWS QuickStart guide](https://gitlab.com/gitlab-org/gitlab-ce/issues/29199) published
* CEO: Preparing GitLab.com to be mission-critical
  * VP Product
    * Improve GitLab.com subscriptions. Storage size increase per subscription level. Ability to upgrade easily.
  * VP Eng
    * Generate and implement plan to align product development with simultaneous CE, EE, and gitlab.com needs
  * Build Lead
    * Build GCP deployment mechanism on Kubernetes for the migration
  * Platform Lead
    * Finish [Circuit breakers](https://gitlab.com/gitlab-org/gitlab-ce/issues/37383)
  * Director of Infrastructure
    * Generate a project plan for the GCP migration and get approved by EJ and Sid
    * Execute milestone 0 of the GCP migration plan by TBD
    * Execute milestone 1 of the GCP migration plan by TBD
    * First byte < 1 second
    * Raise infra/production unit test coverage (branch) from 20% to 80%
    * Reduce infra/production issue backlog from 400 to 200
    * Full database HA (including automated fail-over)
  * Gitaly Lead
    * 100% of Git operations on Gitlab.com go through Gitaly (Gitaly v1.0)
    * Demo Gitaly fast-failure during a file-server outage
  * Database Lead
    * Demo restore time < 1 hour
    * Solve 30% of the schema issues identified by Crunchy
    * Database Uptime 99.99% measured in Prometheus
    * SQL timing under 100ms for Issue, MR, project dashboard, and CI pages measured in Prometheus
  * Director of Security
    * Strong security for SaaS and on-premise product. Top 10 actions from risk assessment done and actions for top 10 risks started.
    * HackerOne bug bounty program. Implemented and bounties awarded.
    * Security policies for cloud services and cloud migrations. Policy published and enacted.
* CMO: Build trust of, and preference for GitLab among software developers
  * CMO: Hire Director, DevRel/Community Relations
    * CMO: Build out DevRel/Community Relations function
    * CA: Grow community and increase community engagement. Increase number of new contributors by 10%, increase number of total contributions per release by 5% and increase number of Twitter mentions of GitLab by 10%.
  * MSD: $600K in self serve revenue.
  * MSD: Grow followers by 20% through proactive sharing of useful and interesting information across our social channels.
  * MSD: Grow number of opt-in subscribers to our newsletter by 20%.
* CMO: Generate more company and product awareness including increasing lead over [BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab)
  * MSD: Implement SEO/PPC program to drive increase in number of free trials by 20% compared to last quarter, increase number of contact sales requests by 22% compared to last quarter, increase amount of traffic to about.gitlab.com by 9% compared to last quarter
  * CMO: PR - October Announcements - 10.0, Series C, Wave, CLA
  * CMO: AR - v10 briefing sweep for targeted analysts

### Objective 3: Great team.

* CFO: Improve team productivity
  * Analytics: Data and Analytics vision and plan signed off by executive team
  * Analytics: Real time analytics implemented providing visual representation of the metrics sheet
  * Legal: Create plan for implementing Global Data Protection and Data Privacy Plan
  * Controller: Reduce time to close from 10 days to 9 days.
  * Accounting Manager: Identify and add to the handbook two new accounting policies.
  * Accounting Manager: Create monthly process for BvsA analysis with department budget owners.
* CCO: Create an Exceptional Corporate Culture / Delight Our Employees
  * Launch training for making employment decisions based on the GitLab Values. Launch by November 15th
  * Launch a short, quarterly Employee Pulse Survey. Strive for 80% completion
  * Analyze and make recommendations based off of New Hire Survey and Pulse surveys which will drive future KRs. Have at least 3 areas to improve each quarter. Ideally, we will also have 3 areas to celebrate.
  * Revise the format of the Morning Team Calls to allow for better participation and sharing. Strive for 80% participation
  * Improve use of the GitLab Incentives by 15%. https://about.gitlab.com/handbook/incentives/
  * Iterate on the Performance Review process with at least two changes initiated by end of year.
* CCO: Grow Our Team With A-Players
  * KR: Socialize and grow participation in our Diversity Referral bonuses by 10% (measurement should be made in January as many hires in December don’t start until January, with awareness that the actual bonuses aren’t paid out for 3 months)
  * More sourced recruiting. 20% of total hires
  * Ensure candidates are being interviewed for a fit to our Values as well as ability to do the job, through Manager Training and Follow-up by People Ops.
  * Hire Recruiting Director
  * 90% of all candidates will be advanced through the pipeline within 7 business days in each phase, maximum.
* CCO: Make All of Our Managers More Effective and Successful
  * Provide consistent training to managers on how to manage effectively. Success will mean that there are at least 15 live trainings a year in addition to curated online trainings.
  * Ensure every manager is doing regular 1-on-1 meetings with 2-way feedback. Measure will be seen in Employee Pulse survey, with at least 90% of employees indicating they have received feedback from their manager in the last month.
  * Hire People Business Partners to partner with managers to operate as leadership coaches, performance management advisors, talent scouts, and Culture/Values evangelists. Goal of 2 hires.
* VPE: Build the best, global, diverse engineering, design, and support teams in the developer platform industry
  * Revise hiring plan for Q1 2018 based on Q4 financials and product ambitions
  * Launch 2018 Q1 department OKRs before EOQ4 2017
  * Hire an additional Director of Engineering
* Support: Grow the support team to better comply with SLAs and cover gitlab.com cases
  * Hire a services support lead
  * Hire an support specialist
  * Hire an EMEA support engineer
  * Hire an EMEA/AMER support engineer
  * Hire an AMER support engineer
* UX: Increase the profile of GitLab design and grow the team
  * Launch first iteration of design.gitlab.com
  * Write 3 public blog posts about GitLab UX and visual design case studies, best practices, anecdotes, or events
  * Hire 2 UX designers
  * Hire a junior UX researcher
* Frontend (DC): Grow the team
  * Hire 3 front end developers
* Frontend (AC): Grow the team
  * Hire 2 front end developers
* Director of Backend: Grow the team
  * Hire a lead for geo
* Build: Grow the team
  * Hire 2 developers
  * Hire a senior developer
* CI/CD: Grow the team
  * Hire 3 developers
  * Hire 2 senior developers
* Discussion: Grow the team
  * Hire 2 developers
* Director of Infrastructure: Grow the team
  * Hire 2 DevOps engineers
  * Hire a database lead
* Gitaly: Grow the team
  * Hire a developer
* Database: Grow the team
  * Hire a database specialist
* Director of Quality: Grow the team
  * Hire a test automation lead
  * Hire 3 test automation engineers
* Director of Security: Build strong team.
  * Hire Security Engineer(s)
  * Hire a Security Specialist Developer
