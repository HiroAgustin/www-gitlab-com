---
layout: markdown_page
title: GitLab for Education
---

GitLab is being widely used by educational institutions around the world.

# Educational Pricing

GitLab’s Educational pricing policy applies to self-hosted GitLab only.

GitLab's Educational pricing is that for educational institutions, students do
not count towards the user count. The purchase of at least one subscription is required.

