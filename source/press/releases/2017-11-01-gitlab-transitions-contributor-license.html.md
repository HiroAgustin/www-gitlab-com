---
layout: markdown_page
title: "GitLab Transitions Contributor Licensing to Developer Certificate of Origin to Better Support Open Source Projects; Empower Contributors"
---

### Open source project customers Debian and GNOME announce plans to migrate communities to GitLab; applaud the direction

**SAN FRANCISCO - November 1, 2017** - GitLab, a software product used by [2/3 of all enterprises](https://about.gitlab.com/is-it-any-good/), today announced it was abandoning the industry-standard Contributor License Agreement (CLA) in favor of a Developer Certificate of Origin (DCO) and license. The DCO gives developers greater flexibility and portability for their contributions. The move has already attracted the attention of large open source projects who recognize the benefits. Debian and GNOME both plan to migrate their communities and open source projects to GitLab.

GitLab’s move away from a CLA is meant to modernize its code hosting and collaborative development infrastructure for all open source projects. Additionally, requiring a CLA became problematic for developers who didn’t want to enter into legal terms; they weren’t reviewing the CLA contract and they effectively gave up their rights to own and contribute to open source code.

“Many large open source projects want to be masters of their own destiny, but overly restrictive licensing can be a barrier to attracting talented contributors and driving innovation in the project,” said Sid Sijbrandij, CEO at GitLab. “With a DCO and license, developers no longer have to surrender their work and enter into legal terms. They will now have the freedom to contribute to open-source code and the flexibility to leverage their contributions as they need.”

In comparison, other open source platforms, like GitHub, Phabricator, Fedora Pagure, Jenkins, and Elastic, all currently require a CLA. For established companies, a shift of this magnitude is not easy. But after evaluating the needs of larger open source projects such as Debian and GNOME, GitLab came to the conclusion that a DCO would better suit their efforts to modernize code hosting and collaborative-development infrastructure.

“We’re thrilled to see GitLab simplifying and encouraging community contributions by switching from a CLA to the DCO,” said Chris Lamb, Debian Project Leader. “We recognize that making a change of this nature is not easy and we applaud the time, patience and thoughtful consideration GitLab has shown here.”

 "We applaud GitLab for dropping their CLA in favor of a more OSS-friendly approach," said Carlos Soriano, Board Director at GNOME. "Open source communities are born from a sea of contributions that come together and transform into projects. This gesture affirmed GitLab's willingness to protect the individual, their creative process, and most importantly, keeps intellectual property in the hands of the creator."

In addition to moving toward DCO, GitLab will take internal actions to review code that is submitted, to help minimize the likelihood of anything problematic entering the base. GitLab has already begun making the switch with no added steps necessary from their user base. For more information on what this means for the broader GitLab community, please visit [this link](https://about.gitlab.com/2017/11/01/gitlab-switches-to-dco-license/).

**About GitLab**  
Since its incorporation in 2014, GitLab has quickly become the leading self-hosted Git repository management tool used by software development teams ranging from startups to global enterprise organizations. GitLab has since expanded its product offering to deliver an integrated source code management, code review, test/release automation, and application monitoring product that accelerates and simplifies the software development process. With one end-to-end software development product, GitLab helps teams eliminate unnecessary steps from their workflow, significantly reduce cycle time and focus exclusively on building great software. Today, more than 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel; and millions of users, trust GitLab to bring their modern applications from idea to production, reliably and repeatedly.

**Media Contact**  
Nicole Plati  
gitlab@highwirepr.com  
415-963-4174 ext. 39
