---
layout: job_page
title: "Senior Product Manager, Continuous Delivery/Release Automation"
---

#### **This job is being moved to [/positions/product-manager/](/positions/product-manager/)**
{: .text-center}
<br>

We're looking for product managers that can help us work on the future of DevOps
tools; specifically, building out continuous delivery (CD), release automation
(RA), and beyond.

We believe developers deserve an intuitive, single application that covers the
complete DevOps lifecycle. We believe running tests is just the beginning of
automation. We believe packaging and deploying your code should be automated and
repeatable. We believe managing and monitoring your applications and
infrastructure should be more than an afterthought. We believe in pushing the
boundaries of best-practices, and bringing these to every developer, so that
doing the right thing is also the easy, default way of working.

We know there are a million things we can and want to improve about GitLab.
It'll be your job to work out what we are going to do and how.

We work in quite a unique way at GitLab, where lots of flexibility and
independence is mixed with a high paced, pragmatic way of working. And
everything we do is in the open.

## Responsibilities

- Find the weak spots of GitLab and execute plans to improve them
- Build an attractive roadmap together with the Head of Product, VP of Product, and CEO; based on our [vision](/direction/#vision)
- Manage new features from conception to market
- Work out feature proposals from the community and customers
- Work together with UX, Frontend, and Backend engineers
- Ensure a smooth release of changes and features together with all stakeholders
- Empower the community by writing great documentation and highlighting the product in various ways in cooperation with marketing

## You are _not_ (solely) responsible for

- Shipping in time. As a PM you are part of a team that delivers a change,
the team is responsible for shipping in time, not you.
- A team of engineers. PMs at GitLab do not manage people, they manage the
_product_. You'll be required to take the lead in decisions about the product,
but it's not your role to manage the people that build the product.
- Capacity and availability planning. You will work together with engineering
managers on schedules and planning: you prioritize, the engineering managers
determine how much can be scheduled.

## Requirements

- Strong Experience in product management
- Strong understanding of Git and Git workflows
- Strong understanding of CI/CD and Release Automation
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes and Docker
- Knowledge of the developer tool space
- Strong technically. You understand how software is built, packaged and deployed.
- Passion for design and usability
- Highly independent and pragmatic
- You are living wherever you want
- You share our [values](/handbook/values), and work in accordance with those values.
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order
below. Please keep in mind that applicants can be declined from the position at
any stage of the process. To learn more about someone who may be conducting the
interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Head of Product
* Candidates will then be invited to schedule an interview with the CI/CD Lead
* Candidates will be invited to schedule a third interview with our VP of Product
* Candidates may be asked to meet with additional team members at the manager's discretion
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
