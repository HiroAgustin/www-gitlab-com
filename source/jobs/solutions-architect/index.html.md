---
layout: job_page
title: "Solutions Architect"
---

Solution Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements. Solution Architects are responsible for actively driving and managing the technology evaluation and validation stages of the sales process. Solution Architects are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the the technical solution while also understanding the business challenges the customer is trying to overcome.

The Solution Architect for Customer Success Initiatives provides the opportunity to help drive value and change in the world of software development for one of the fastest growing platforms. By applying your solution selling and architecture experience from idea to production, you will support and enable successful adoption of the GitLab platform. You will be working directly with our top enterprise customers. You will be working collaboratively with our sales, engineering, product management and marketing organizations. This role provides technical guidance and support through the entire sales cycle. You will have the opportunity to help shape and execute a strategy to build mindshare and broad use of the GitLab platform within enterprise customers becoming the trusted advisor. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting. The ability to connect technology with measurable business value is critical to a solutions architect. You should also have a demonstrated ability to think strategically about business, products, and technical challenges.


## Responsibilities

* Primarily engaged in a pre sales technical consultancy role, providing technical assistance and guidance specific to enterprise level implementations, during the pre sales process by identifying customers technical and business requirements in order to design a custom GitLab solution
* In partnership with the sales team, formulate and execute a sales strategy to exceed revenue objectives through the adoption of GitLab
* Educate customers of all sizes on the value proposition of Gitlab, and participate in all levels of discussions throughout the organization to ensure our solution is setup for successful deployment
* Work on­site with strategic, enterprise­ class customers, delivering solutions architecture consulting, technical guidance, knowledge transfer and establish “trusted advisor status”
* Capture and share best-practice knowledge amongst the GitLab community
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, GitLab Handbook
* Build deep relationships with senior technical individuals within customers to enable them to be GitLab advocates
* Serve as the customer advocate to other GitLab teams including Product Development, Sales, and Marketing
* Present GitLab platform strategy, concepts, and roadmap to technical leaders with the customer’s organization

## Requirements

* Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers.
* Strong written communication skills
* High level of comfort communicating effectively across internal and external organizations
* Ideal candidates will preferably have 7 plus years IT industry or IT technical sales related to experience with a proven track record of solution sales expertise
* Significant experience with executive presence and a proven ability to lead and facilitate executive meetings and workshops.
* Deep knowledge of software development lifecycle and development pipeline (idea to production)
* Understanding of continuous integration, continuous deployment, chatOps, and cloud native
* Experience with waterfall, Agile (SCRUM, Kanban, etc) and able to discuss the value of different software development processes
* Experience with container systems preferred
  * Kubernetes / Docker
  * Installation / configuration
  * Container registries
* Experience with any of the following tools / software packages preferred:
  * Ruby on Rails Applications
  * Git,BitBucket/Stash, GitHub, Perforce, SVN
  * Jira, Jenkins
* Understand mono-repo and distributed-repo approaches
* Ability to travel up to 30%
* B.Sc. in Computer Science or equivalent experience
- Successful completion of a [background check](/handbook/people-operations/#background-checks).


## Preferred Skills/Qualifications

* Above average knowledge of Unix and Unix based Operating Systems
  * Installation and operation of Linux operating systems and hardware investigation / manipulation commands
  * BASH / Shell scripting including systemd and init.d startup scripts
  * Package management (RPM, etc. to add/remove/list packages)
  * Understanding of system log files / logging infrastructure



## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 45min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule interviews with Director of Customer Success
* The strongest candidates may be asked to schedule additional 45 minute interviews with an Account Executive, a Regional Sales Director, and our CRO
* Finally, candidates may interview with our CEO

## Compensation

You will typically get 80% as base and 20% based on commission. 
Also see the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).
