---
layout: markdown_page
title: "Lever"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## How to join Lever

[Lever](hire.lever.co/) is GitLab's ATS (Applicant Tracking System). All hiring managers and interviewers will use Lever to review resumes, provide feedback, communicate with candidates, and more.

When you [log in to Lever](https://hire.lever.co/auth/login) for the first time, choose "Sign in with Google", making sure you're already logged in to your GitLab email account. All GitLab team members can log in to [Lever](hire.lever.co) and be granted the “Interviewer” role, the most basic [access role](https://help.lever.co/hc/en-us/articles/205644649). Until their access is upgraded by an admin, they will only have access to one page in Lever, [Interviews](https://hire.lever.co/interviews), where employees can complete interview feedback and make referrals. If you need your access to be upgraded so you can review candidate profiles, please reach out to People Ops.

You can read more about [how to log in to Lever here](https://help.lever.co/hc/en-us/articles/204485965-How-do-I-log-in-). Please note that Lever only officially supports Chrome on desktop. While they are actively working on optimizing Lever across browsers, they recommend using Lever on Chrome.

## How to set up your Lever account

After joining Lever, it is important for all interviewers and hiring managers to connect their Google and Slack accounts to Lever to optimize their experience and the use of the platform.

### Sync Google account

Please read this article on [how to sync your Google account to Lever](https://help.lever.co/hc/en-us/articles/204502445-How-does-Lever-s-Gmail-sync-work-).
Syncing your Google account (both your email and your calendar) allows for seamless communication with candidates and coworkers, as well as a [Lever scheduling feature called Easy Book](https://help.lever.co/hc/en-us/articles/115003800823-How-do-I-schedule-an-interview-using-Lever-s-Easy-Book-feature-), where each candidate will be given a unique link to your calendar in order to schedule an interview.


#### Scheduling interviews with Easy Book

The recruiting team will schedule all interviews and send out unique Easy Book links, along with a link to a Google Hangouts room for each interviewer, which will follow this format: `hangouts.google.com/hangouts/_/gitlab.com/xlastname`  (e.g. Joe Smith's would be: `hangouts.google.com/hangouts/_/gitlab.com/jsmith`). This room will be your consistent location for interviews, but it will not show up in your calendar invite. We recommend you bookmark this link for easy access.

If you use another standard link for a room using a different service (Zoom, Appear.in, etc.), please email the link for that room to [Nadia Vatalidis](https://about.gitlab.com/team/#nadiavat), [Chloe Whitestone](https://about.gitlab.com/team/#drachanya), and [Jacie Zoerb](https://about.gitlab.com/team/#jaciezoerb).

#### Important Notes
* Please be sure to go to your [Google Calendar settings](https://calendar.google.com/calendar/render?tab=mc#settings-general_11) and input your current time zone.
* Easy Book will only show candidates times you are available within the hours of 9am-5pm your local time (per the above point). If you would like to only have interviews during a specific period of your day, please block off the rest of your day so Easy Book will not offer those times. Reach out to People Ops if you have questions on this.

### Sync Slack account

Please read this article on [how to sync your Slack account to Lever](https://help.lever.co/hc/en-us/articles/115000574666). The Slack integration will send you updates about upcoming interviews ten minutes before the interview starts. It will include the candidate information as well as a link to view their resume/profile and to leave feedback.

## How to view candidates in Lever

When you log in to Lever, you will be brought to the [Candidates page](https://hire.lever.co/candidates). At the top left corner, you can filter which candidates you are looking at. It's recommended to select "Followed Candidates". Read more about [the difference between the All owners, Owned by me, and Followed filters](https://help.lever.co/hc/en-us/articles/208223936-What-is-the-difference-between-the-All-owners-Owned-by-me-and-Followed-filters-).

It is helpful to understand [what "following" is](https://help.lever.co/hc/en-us/articles/203824939-What-is-following-) in order to best make use of the function. If you would like to follow a candidate or role that you do not currently have access to, please reach out to People Ops.

If you are on the go and would like to access Lever via your mobile device, please see this article: [How to view a candidate on Mobile](https://help.lever.co/hc/en-us/articles/218355523-How-can-I-view-candidate-profiles-on-a-mobile-device-).

## How to leave feedback in Lever

After you've integrated Slack with Lever, you'll receive a Slack message reminding you of your interview, with a link to the candidate's resume. When you click that link, it will show you the details for that particular interview. Please click "Feedback Form" under their name, and track your feedback in the text box(es) during your call.

You can also go straight to the [Interviews page](https://hire.lever.co/interviews), where you will see all of your upcoming interviews and be able to click into each one to see the details and provide feedback, as well as keep track of your past and future interviews. All team members who have logged in to Lever have access to this page.

Alternatively, if you have access to candidate profiles, you can go directly to their profile to give feedback. Next to the "Add Note" field at the top, click on the three dots "..." and choose "Add Feedback". Then choose the "General Feedback Form" and fill out the text box(es). You will be required to provide a vote of "Inclined", "Semi-Inclined", or "Not Inclined." You will also be asked to give an overall rating on a scale of 4 (4 = Strong Hire; 3 = Hire; 2 = No; 1 = Strong No).

There is an option to make feedback forms, notes, and emails "secret," but please leave all information public, unless it refers to sensitive candidate data such as compensation. Only People Ops team members and hiring managers should be discussing compensation information with candidates, so they should be the primary users of secret notes.

## How to make a referral

You can [refer someone directly](https://hire.lever.co/referrals/new) by providing their name, email address, what position(s) you would like to refer them for, as well as other relevant details, such as a LinkedIn profile, phone number, and any notes you have. Please note it is required to include their email address.

You may also create a referral link, which you can send to prospective candidates in your network, and you will automatically be credited as their referral. To do so, go to the [Interviews page](https://hire.lever.co/interviews), click on "Social Referrals", and click "Get referral link". You can share this link to your social networks directly and/or copy the link to share elsewhere.

Please see these Lever articles on [how to make a referral](https://help.lever.co/hc/en-us/articles/204493415-How-do-I-make-a-referral-) and [social referrals](https://help.lever.co/hc/en-us/articles/205697609-What-are-social-referrals-) for more information, as well as GitLab's [referral bonus program](https://about.gitlab.com/handbook/incentives/#referral-bonuses).

## Additional Lever information
* [Dashboard Overview](https://lever.wistia.com/medias/1pv5mqnrps)
* [Candidate Profile Overview](https://lever.wistia.com/medias/5yq8pamndg)
* [What are the keyboard shortcuts and hotkeys in Lever?](https://help.lever.co/hc/en-us/articles/205633775-What-are-the-keyboard-shortcuts-and-hotkeys-in-Lever-)
* [How does search work in Lever?](https://help.lever.co/hc/en-us/articles/206403245-How-does-search-work-in-Lever-)
* [How do I add an emoji to a note in Lever?](https://help.lever.co/hc/en-us/articles/208337403-How-do-I-add-an-emoji-to-a-note-in-Lever-)
* [What does Lever have access to on my Google account?](https://help.lever.co/hc/en-us/articles/203825829-What-does-Lever-have-access-to-on-my-Google-account-)
* [How does Lever handle viruses in candidate files?](https://help.lever.co/hc/en-us/articles/115001571243-How-does-Lever-handle-viruses-in-candidate-files-)
* [How do I change my profile picture (avatar)?](https://help.lever.co/hc/en-us/articles/203825799-How-do-I-change-my-profile-picture-avatar-)
* [What's the difference between a Published, Internal, Closed, and Draft job posting?](https://help.lever.co/hc/en-us/articles/205066609-What-s-the-difference-between-a-Published-Internal-Closed-and-Draft-job-posting-)
