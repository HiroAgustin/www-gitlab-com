---
layout: markdown_page
title: "Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

NOTE: Our contractor agreements and employment contracts are all on the [Contracts](https://about.gitlab.com/handbook/contracts/) page.

## Entity Specific Benefits
- [GitLab BV (Netherlands)](/handbook/benefits/bv-benefits-netherlands)
- [GitLab BV (Belgium)](/handbook/benefits/bv-benefits-belgium)
- [GitLab Lyra (India)](/handbook/benefits/lyra-benefits-india)
- [GitLab Inc (US)](/handbook/benefits/inc-benefits-us)
- [GitLab Inc (China)](/handbook/benefits/inc-benefits-china)
- [GitLab LTD (UK)](/handbook/benefits/ltd-benefits-uk)
- TODO GitLab GmbH

## Guiding Principles

These principles will guide our benefit strategies and decisions.

* **Collaboration**
  - Work with providers, vendors, and global networks to benchmark benefit data from other similar sized companies.
  - Foster cross-company understanding.
* **Results**
  - Evangelize benefit programs in each entity.
  - Transform “statutory” to “competitive.”
  - Make benefits a very real aspect of compensation during the hiring process.
  - Measure employee engagement and benefit enrollment.
* **Efficiency**
  - Use the GitLab Inc (US) benefits as a starting point, and try to stay as consistent as possible within the US baseline.
  - Iterate on changes for specific countries based on common practices and statutory regulations.
* **Diversity**
  - Actively work to ensure that our benefit choices are inclusive of all team members.
* **Iteration**
  - Regularly review current benefits (bi-annually) and propose changes.
  - Everything is always in draft.
* **Transparency**
  - Announce and document benefits to keep them updated.
  - Share upcoming benefit plans internally before implementing them.
  - Invite discussion and feedback.

We value opinions but ultimately People Operations/Leadership will make the decision based on expert advice and data.

## General Benefits

For the avoidance of doubt, the benefits listed below in the General Benefits section are available to contractors and employees, unless otherwise stated. Other benefits are listed by countries that GitLab has established an entity or co-employer and therefore are applicable to employees in those countries only. GitLab has also made provisions for Parental Leave which may apply to employees and contractors but this may vary depending on local country laws. If you are unsure please reach out to the People Operations team.

1. GitLab will pay for the items listed under [spending company money](https://about.gitlab.com/handbook/spending-company-money).
1. [Stock options](/handbook/stock-options/) are offered to most GitLabbers.
1.  Deceased team member:
    In the unfortunate event that a GitLab team member passes away, GitLab will
    provide a $20,000 lump sum to anyone of their choosing. This can be a spouse,
    partner, family member, friend, or charity.
      * For US based employees of GitLab Inc., this benefit is replaced by the
        [Basic Life Insurance](/handbook/benefits/inc-benefits-us/#basic-life-insurance-and-add) that is offered through TriNet.
      * For all other GitLabbers, the following conditions apply:
         * The team member must be either an employee or direct contractor,
         * The team member must have indicated in writing to whom the money
           should be transferred. To do this you must complete this [expression of wishes](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing) form, email to People Ops, who will then file in BambooHR.
         * For part-time GitLabbers, the lump sum is calculated pro-rata, so
           for example for a team member that works for GitLab 50% of the time,
           the lump sum would be $10,000.
1. [Paid time off policy](https://about.gitlab.com/handbook/paid-time-off).
1. [Tuition Reimbursement](https://about.gitlab.com/handbook/people-operations/#tuition-reimbursement)
1. [GitLab Summit](https://about.gitlab.com/culture/summits)
   * Every nine months or so GitLabbers gather at an exciting new location to [stay connected](https://about.gitlab.com/2016/12/05/how-we-stay-connected-as-a-remote-company/), at what we like to call a GitLab Summit. It is important to spend time face to face to get to know your team and, if possible, meet everyone who has also [bought into the company vision](http://www.excitingrole.com/how-to-do-startup-due-diligence/#.h/). There are fun activities planned by our GitLab Summit Experts, work time, and presentations from different functional groups to make this an experience that you are unlikely to forget! Attendance is optional, but encouraged. For more information and compilations of our past summits check out our [summits page](https://about.gitlab.com/culture/summits).
1. [Business Travel Accident Policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing)
   *  This policy provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days.
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims or questions, please contact the [People Operations Specialist](https://about.gitlab.com/team/#brittany_rohde).
   * This policy will not work in conjunction with another personal accident policy as the Business Travel Accident Policy will be viewed as primary and will pay first.
1. [Immigration](/handbook/people-operations/visas/) Benefits for eligible team members.
1. [Incentives](https://about.gitlab.com/handbook/incentives) such as
   - [Sales Target Dinner Evangelism Reward](https://about.gitlab.com/handbook/incentives/#sales-target-dinner)
   - [Discretionary Bonuses](https://about.gitlab.com/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](https://about.gitlab.com/handbook/incentives/#referral-bonuses)
   - [Travel Grant](https://about.gitlab.com/handbook/incentives/#travel-grant)

1. Possible Future Benefits: [Wellness Program](https://gitlab.com/gitlab-com/peopleops/issues/317).

## Parental Leave

Anyone (regardless of gender) at GitLab who becomes a parent through childbirth or adoption is able to take fully paid parental leave. GitLabbers will be encouraged to decide for themselves the appropriate amount of time to take and how to take it. We will offer everyone up to 12 weeks of 100% paid time off during the first year of parenthood. We encourage parents to take 8-12 weeks.

For many reasons, a team member may require more time off for parental leave. Many GitLabbers are from countries that have longer standard parental leaves, occasionally births have complications, and sometimes 12 weeks just isn't enough. Any GitLabber can request additional unpaid parental leave, up to 4 weeks. We are happy to address anyone with additional leave requests on a one-on-one basis. All of the parental leave should be taken in the first year.

If you have been at GitLab for a year your parental leave is fully paid. If you've been at GitLab for less than a year it depends on your jurisdiction.

You are entitled to and need to comply with your local regulations. They override our policy.

If you need any additional leave, please refer to our [Paid Time Off Policy](https://about.gitlab.com/handbook/paid-time-off).

### Country Specific Leave Requirements

All country specific requirements are subject to change as legal requirements change.

#### GitLab Inc. United States Leave Policy:

Extended Leave Regulations:  

* Based on the Family and Medical Leave Act, or [FMLA](https://www.dol.gov/whd/fmla/), US Employees are "eligible to take job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the employee had not taken leave." For more information on what defines an eligible employee and medical reason please visit the [Electronic Code of Federal Regulation](http://www.ecfr.gov/cgi-bin/text-idx?c=ecfr&sid=d178a2522c85f1f401ed3f3740984fed&rgn=div5&view=text&node=29:3.1.1.3.54&idno=29#sp29.3.825.b) for the most up to date data.
* US Employees are also eligible to take [Short-Term Disability](/handbook/benefits/#short-and-long-term-disability-insurance) based on the benefits plan through TriNet for qualifying events based on our Parental Leave Policy.   

To Apply For Extended Leave:

* Fill out the [Extended Leave Request Form](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPdzhITFI2VDR2TVAxS2N6ZUlkVzZveTBWSkVR/view?usp=sharing) and send to People Ops. Once submitted, People Ops will review and submit to TriNet.

##### Entering an Extended Leave in TriNet

1. Once the Extended Leave Request form has been submitted and approved by People Ops, it will then be entered into TriNet.
1. Go to Passport under Find > Find Person by Name > Pull the employee up > Extended Leave Request.
1. Enter all applicable information.
1. Once the leave is in TriNet it takes the LOA department about 5 business days to process the request. They will send out information to the employee directly on what their leave means to them and how to file for any short term and/or state disability that may apply.
  * Short Term Disability is processed through Aetna. The employee would call Aetna to file a claim. Phone number and plan number can be found in the People Ops 1Password vault to be shared with the employee.

##### Entering a Return from an Extended Leave in TriNet

1. On the team member's first day back from leave, confirm that the employee has returned. A request will need to be submitted to TriNet for the return as well as a special payroll for any days that were not paid if a payroll has already been processed. 
1. In the Beta site click on your Admin View at the top right. 
1. Then in the menu to the left choose Employees > Manage Employees.  
1. Search and click on the employee returning from leave.  
1. Then click the Return from Leave button at the top right.  
1. Input all details and submit.  

#### GitLab Inc. China Leave Policy:

There are many regulations and policies around different types of leave in China. Different regions have separate statutes, and it is also influenced by many factors, such as the employee’s work period and actual situation from the hospital. CIIC will provide the support when the featured employee is in need.

#### GitLab B.V. Netherlands Leave Policy:

* Statutory Maternity leave
  - The employee is entitled to a maximum six weeks' leave prior to the estimated date of childbirth and for ten weeks after that date; therefore totaling sixteen weeks.
  - The employee can reduce the leave period prior to the estimated date of childbirth to at least four weeks. In that case, the number of days not taken prior to the estimated date of childbirth is added to the leave period following the estimated date of childbirth.
  - In the event of incapacity for work from six weeks prior to the estimated date of childbirth, the sixteen-week period for pregnancy and childbirth leave commences at that time, regardless of which agreements have been made.

* Statutory Paternity Leave
  - After your partner has given birth you are entitled to up to two days of paid paternity leave.

* Statutory Parental leave
  - Employees who have children under the age of eight are entitled to take unpaid parental leave. In the case of a family with more than one child under the age of eight, that right is applicable for each child. The number of hours' leave is thirteen times the weekly working hours (65 days for full-time employment). However, no more than half of the number of weekly working hours can be taken each week.

People Ops will consult with HRSavvy to ensure that the statute is met.

##### Applying for Leave in the Netherlands

HRSavvy can assist in applying for maternity leave covered by social security. In this application the company can decide whether the benefit is paid to the employee directly, or the employer continues paying the salary and receives the benefit. The last option is done in most of the cases.

#### GitLab B.V. Belgium Leave Policy:

* Statutory Parental Leave
  - Maternity leave is 15 weeks, which can be split in prenatal leave, one week before the delivery date. Postnatally leave is at least nine weeks, allowing the mother to choose five weeks, either as prenatal as postnatally.
  - Dads are given 10 days, three of which are 100% paid. The remaining seven can be used during the baby's first four months.
  - Parental leave should be requested by the employee and registered by three months before the start of leave.

People Ops will consult with Vistra to ensure that the statute is met.

#### GitLab B.V. India Leave Policy:

* Statutory Maternity Leave
  - Women employees are eligible to claim 26 weeks paid maternity leave as per Maternity Benefit Act 1961. This can be availed maximum on two occasions only. A fitness certificate should be submitted at the time of resumption of duties.
  - Every women employee availing maternity leave is eligible for an insurance coverage of INR 50,000 for normal/C-section.
  - If employee wishes to extend her maternity leave, she shall make a written request to her Reporting Manager and People Operations with a valid justification. In such situation Reporting Manager and People Operations will review the case and inform employee to utilize her SL, CL and PLs for payable days OR / up to a maximum of 4 weeks from 90th day with loss of pay.

* Statutory Paternity Leave:  
  - All confirmed male employees are eligible for availing Paternity Leave not exceeding 3 consecutive working days (excluding holidays) up to 2 children. These leaves are paid leaves.

People Ops will consult with Lyra to ensure that the statute is met.

#### GitLab LTD United Kingdom Leave Policy:

* Statutory Maternity Leave
  - Mother can take up to a year. Six weeks is paid at the average income over the proceeding 18 weeks (includes commissions, variable pay, etc). The employee is paid at the statutory requirement over the next 33 weeks, and the remaining 13 weeks of the year is unpaid.

* Statutory Paternity Leave:  
  - Paternity - Father is paid for two weeks at the standard rate, but can now share some of the mother’s 52 week period.

The mother may opt to share some of her 52 week maternity leave with her spouse/partner (so long as it is not the first 2 weeks following birth).  The leave period will be paid in accordance with the above regulations for Maternity (ie maximum combined leave of 52 weeks, payments for up to 39 weeks at Statutory Pay rate).
Employees accrue all benefits during leave and must have documentation for risk assessment when notifying the company of pregnancy.

Once an employee notifies GitLab of the pregnancy, People Ops will conduct:
1. [Risk Assessment](https://docs.google.com/document/d/1qHdbaeFSnqdwkQDTEurHD5QMbLPLuZGBQO_CBnRkpiE/edit) – carry out a risk assessment of her work environment within 1-3 days of notification. This still applies if the employee works from home. The purpose of the assessment is to make the employee aware of any hazards or “risks” to her in the work place/home office to reduce potential injury to her or her baby.
1. Notify the employee of the statutory rules/company practice regarding leave, pay, notifications etc with a link to this section of the handbook.

People Ops will consult with Vistra to ensure that the statute is met.
