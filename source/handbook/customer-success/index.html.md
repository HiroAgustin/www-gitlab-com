---
layout: markdown_page
title: "Customer Success"
---
# Welcome to the Customer Success Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

## Role Description
GitLab Customer Success is made up of Solution Architects.  Solution Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements. Solution Architects are responsible for actively driving and managing the technology evaluation and validation stages of the sales process.  There are [three specialist types](#customer-success-specialities) within the Customer Success group.


## Responsibilities
* Primarily engaged in a technical consultancy role, providing technical assistance and guidance specific to enterprise level implementations, during the pre-sales process by identifying customers technical and business requirements in order to design a custom GitLab solution.
* In partnership with the sales team, formulate and execute a sales strategy to exceed revenue objectives through the adoption of GitLab.
* Educate customers of all sizes on the value proposition of Gitlab, and participate in all levels of discussions throughout the organization to ensure our solution is setup for successful deployment
* Work on­site with strategic, enterprise­ class customers, delivering solutions architecture consulting, technical guidance, knowledge transfer and establish “trusted advisor status”
* Capture and share best-practice knowledge amongst the GitLab community
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, GitLab Handbook
* Build deep relationships with senior technical individuals within customers to enable them to be GitLab advocates
* Serve as the customer advocate to other GitLab teams including Product Development, Sales, and Marketing
* Present GitLab platform strategy, concepts, and roadmap to technical leaders with the customer’s organization

## Mission Statement
To deliver value to all customers by engaging in a consistent, repeatable, scalable way across defined segments so that customers see the value in their investment with GitLab, and we retain and drive growth in our within our enterprise customers. 

* The mission of the solutions architect team is to provide these customers with experience in order to:
    * Accelerate initial customer value
    * Maximize long-term, sustainable customer value
    * Improve overall customer satisfaction & referenceability
    * Maximize the total value of the customer to GitLab

    
## Team Initiatives
Our large and strategic customers are in need of an ongoing partnership that combines expert guidance with flexibility and adaptability to support their adoption and continuous improvement initiatives.  

These customers expect that partner to provide a streamlined, consistent and fully coordinated experience that encompasses the full span of their needs as well as the fully lifecycle of the relationship.
Need to focus on 3 main areas in order to grow in our existing accounts as well as land large and strategic:

1. Awareness
2. Adoption
3. Usage


### Initiative: Awareness
Opportunity to improve the overall awareness of GitLab in order to promote and evangelize our brand and solution in a meaningful way to provide big business impact to our customers so that they beleive in our vision and strategy.

### Initiative: Adoption
Ensuring paying customers are successful in their onboarding in order to gain adoption and get the most out of our platform and remain happy, paying GitLabers and brand advocates.

### Initiative: Usage
Collecting and making use of customer data and insights is key to customer success.  It’s important to do more with data and when necessary share back with customers, which in turn helps and encourages our customers to improve and drive adoption.

## Customer Success Specialities
### Pre-sales Solutions Architects
**Pre-Sales Solution Architects** are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the technical solutions while also understanding the business challenges the customer is trying to overcome.

* [ When and How to Engage a Pre- Sales Solutions Architect](/handbook/customer-success/engaging-pre-sales-solutions-architect)

### Professional Services
**Professional Services Implementation Specialist** have the goal to optimize your organization's adoption of GitLab through our implementation services, designed to enable other necessary systems in your environment as they are used by your teams moving code from idea to production.

### Customer Success Manager
**Customer Success Manager** help our large organizational customers continue to drive adoption of cloud-native SDLC best practices throughout their tenure as a GitLab customer.  Focused on the journey to Complete DevOps, TAMs will be with customers even after implementation to continue to advise and adapt to customers changing needs.

## Other Resources
### Customer Success resource links outside handbook

* [Solutions Architects Onboarding Bootcamp](https://about.gitlab.com/handbook/customer-success/solutions-architects-onboarding-bootcamp/)
* [HealthCheck](https://docs.google.com/document/d/1aHA3W2FsHUApnz2XVtJoyhpcGYy6bgOHoRi4ArXnF0o/edit) Internal Only
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [GitLab University](https://docs.gitlab.com/ce/university/)
* [Our Support Handbook](https://about.gitlab.com/handbook/support/)
* [GitLab Hosted](https://about.gitlab.com/gitlab-hosted/)
* [Workflow SA Demo Scenarios](https://docs.google.com/document/d/1kSVUNM4u6KI8M9FxoyiUbHEHAHIi34iiY25NhMxLucc/edit) Internal Only

### Other Sales Topics

* [Sales HandBook](https://about.gitlab.com/handbook/sales/)
* [Sales Standard Operating Procedures](http://about.gitlab.com/handbook/sales/sop/)
* [Sales Operations](https://about.gitlab.com/handbook/sales/salesops/)
* [Sales Skills Best Practices](https://about.gitlab.com/handbook/sales-training/)
* [Sales Discovery Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
* [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
* [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
* [FAQ from prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
* [CLient Use Cases](https://about.gitlab.com/handbook/use-cases/)
* [POC Template](https://handbook/sales/POC/)
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24))
* [Sales Demo](/handbook/sales/demo/)
* [Idea to Production Demo](/handbook/product/i2p-demo)
* [Sales Development Team Handbook](/handbook/sales/sdr)
* [Who to go to to ask Questions or Give Feedback on a GitLab feature](https://about.gitlab.com/handbook/product/#who-to-talk-to-for-what)
* [CEO Preferences when speaking with prospects and customers](https://about.gitlab.com/handbook/people-operations/ceo-preferences/#sales-meetings)

### Customer On-boarding Process

This content has moved to the [Account Management Handbook](/handbook/account-management/)

### Customer Success & Market Segmentation
For definitions of the account size, refer to [market segmentation](/handbook/sales#market-segmentation) which is based on the _Potential EE Users_ field on the account.

- Strategic: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Large: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Mid-Market: Account Executive closes deal and transfers to Account Manager to handle renewals and growth. The Account Manager records the Previous Account Owner on the Opportunity Team in any deals while the account is in the New Customer period within the first 12 months including the first annual renewal.
- SMB: Web Portal with Sales Admin oversight. 