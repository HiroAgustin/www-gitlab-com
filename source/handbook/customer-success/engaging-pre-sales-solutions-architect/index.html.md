---
layout: markdown_page
title: "Engaging a Pre-Sales Solutions Architect"
---

## When and How to Engage a Pre-Sales Solutions Architect
Engaging GitLab Solutions Architects

### Before getting SA involved:

1) Qualification:
* What is the qualified reason to engage with us?
* What is the size/quality/state of the opportunity?
*Have you created your plan?

2) Preparation:

Help us, help them.  3 ASKs: 
* Outcome.
* Infrastructure.
* Challenges.

The SA should either be included in a discovery call or provided with Outcome / Infrastructure / Challenges (see below) information uncovered by the AM in prior interactions with the account.  Occasionally, we could support a combination call of discovery and technical demonstration/deep-dive, but this is suboptimal.  However, the latter approach does not allow the SA time to prepare for and/or tailor the discussion.

**Outcome**
WIIFM/T.  What’s in it for them?  Why are they looking at a new strategy for s/w dev.  We need to be able to tailor our demos and presentations to demonstrate value and how we can address their issues.

**Infrastructure**
What i2p tools do they currently have in place?  E.g. Jenkins for CI, Ansible/Chef for automation, Codebear for review, VS/eclipse/emacs/vim for dev, etc.

**Challenges**
What problems/roadblocks are they having?  Release velocity, visibility, collaboration, automation.


3) Engagement Protocol:

First, Navigate to the SA Service Desk Board [here](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/boards/339477)

> NOTE: The board is located in the Group “Customer Success” and the name of the project is “SA Service Desk”

1. Create a [new issue](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
1. Use the "SA Activity" template to ensure the proper data is collected about your needs
1. Add the “SA Backlog” label to the Issue
1. The SA Team will triage the SA Backlog queue and collaborate with the submitter via Issue Discussions.

> NOTE: This will not replace SFDC.  The SAs will still be required to input the necessary information for each account that is critical to the strategy

* [Video: How to engage Solutions Architects using GitLab](https://drive.google.com/file/d/0BztS4JxtaDlrTnZIcWFhb0dWZ2c/view?ts=59879f01) Internal Only


