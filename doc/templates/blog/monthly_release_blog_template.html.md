---
release_number: "X.Y" # feature number - required
title: "GitLab X.Y Released with Feature A and Feature B" # short title - required
author: Joshua Lambert # author name ans surname - required
author_gitlab: joshlambert # author's gitlab.com username - required
author_twitter: # author's twitter username - optional
image_title: '/images/X_X/X_X-cover-image.jpg' # cover image - required
description: "GitLab X.Y Released with Feature A, Feature B, Feature C, Feature D and much more!" # short description - required
twitter_image: '/images/tweets/gitlab-X-X-released.png' # social sharing image - not required but recommended
categories: release # required
layout: release # required
---

<!--

This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/

-->

Introductory paragraph.

<!-- more -->

Introduction.

[Markdown](/handbook/product/technical-writing/markdown-guide/) supported.
